import * as types from '../actiontypes/session';
import * as constant from "../../components/constant";
import {
    Alert
} from 'react-native';

export const loginUser = (Username, password, company) => {
    return (dispatch) => {
        dispatch(sessionLoading())
        // var ciphertext = CryptoJS.AES.encrypt(userInfo.username, 'secret key 123').toString();
        // var bytes = CryptoJS.AES.decrypt(ciphertext, 'secret key 123');
        // var originalText = bytes.toString(CryptoJS.enc.Utf8);
        fetch(`${constant.domain}/login/`, {
            method: 'POST',
            header: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: Username,
                password: password,
                company: company
            })
        })
            // .then((response) => response.json())
            .then(response => {
                console.log(JSON.stringify(response, null, 4))
                return response.json()
            })
            .then((res => {
                if (res.status == 1) {
                    dispatch(sessionSuccess(res))
                }
                else {
                    // this.refs.progress.finish();
                    Alert.alert(
                        '',
                        'Please input your information correctly',
                        [
                            { text: 'OK', onPress: () => console.log('OK Pressed') },
                        ],
                        { cancelable: false }
                    )
                }
            })
            ).done();
    }
}

export const updateData = (res) => {
    return (dispatch) => {
        dispatch(sessionSuccess(res))
    }
}

const sessionRestoring = () => ({
    type: types.SESSION_RESTORING
})
//--
const sessionLoading = () => ({
    type: types.SESSION_LOADING
})

const sessionSuccess = res => ({
    type: types.SESSION_SUCCESS,
    res
})

const sessionError = error => ({
    type: types.SESSION_ERROR,
    error
})

const sessionLogout = () => ({
    type: types.SESSION_LOGOUT
})
  //--