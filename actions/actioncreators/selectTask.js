import * as types from '../actiontypes/selectTask';


export const updatetask = (task) => {
    return (dispatch) => {
        dispatch(sessionLoading())
        dispatch(Success(task))
        
    }
}

const sessionLoading = () => ({
    type: types.SELECTEDTASK_LOADING
})

const Success = task => ({
    type: types.SELECTEDTASK_SUCCESS,
    task
})
