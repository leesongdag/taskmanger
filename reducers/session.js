import * as types from '../actions/actiontypes/session'

const initialState = {
  restoring: false,
  loading: false,
  res: null,
  pinState:false,
  error: null,
  GroupInfo: null,
  LeadChaperoneList:[],
  TourGuideList: []
}

const session = (state = initialState, action) => {
  switch (action.type) {

    case types.SESSION_RESTORING:
      return { ...state, restoring: true }
    case types.SESSION_LOADING:
      return { ...state, restoring: false, loading: true, error: null }
    case types.SESSION_SUCCESS:
      return {  ...state, restoring: false, loading: false, res: action.res, error: null }
    case types.SESSION_ERROR:
      return { ...state, restoring: false, loading: false, error: action.error }
    case types.SESSION_LOGOUT:
      return initialState
    default:
      return state
  }
}

export default session
