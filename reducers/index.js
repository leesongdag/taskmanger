import { combineReducers } from 'redux';

import chat from './chat'
import session from './session'
import selectedtask from './selectedtask'
import countReducer from './countReducer';
import notification from './Notification';
const rootReducer = combineReducers({
  chat,
  session,
  countReducer,
  notification,
  selectedtask
});

export default rootReducer;
