import * as types from '../actions/actiontypes/selectTask'

const initialState = {
  loading: false,
  task: [],
  error: null,
}

const session = (state = initialState, action) => {
  switch (action.type) {
   case types.SELECTEDTASK_LOADING:
      return { ...state,  loading: true, error: null }
    case types.SELECTEDTASK_SUCCESS:
      return {  ...state,  loading: false, task: action.task, error: null }
    case types.SESSION_ERROR:
      return { ...state,  loading: false, error: action.error }
    default:
      return state
  }
}

export default session
