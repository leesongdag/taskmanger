import * as types from '../actions/actiontypes/Notification'

const initialState = {
    sending: false,
    sendingError: null,
    status: false
}

const chat = (state = initialState, action) => {
    switch (action.type) {
        case types.NOTIFICATION_LOADING:
            return { ...state, sending: true, status: false, sendingError: null }
        case types.NOTIFICATION_SUCCESS:
            return { ...state, status: true, sending: false }
        case types.NOTIFICATION_ERROR:
            return { ...state, status: false, sending: false, sendingError: action.error };
        default:
            return state
    }
}

export default chat