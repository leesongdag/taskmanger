import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Dimensions,
    Image,
    View,
    StyleSheet,
    TouchableOpacity,
    Text
} from 'react-native';
import './util/constant'

class dashboard extends Component {
    static navigationOptions = {
        title: null,
        header: null,
    };
    constructor(props) {
        super(props)
        const { result } = this.props
        this.state = {
            user_information: '',
            Username: '',
            password: '',
            result: result,
            task_data: result.tasks,
            start: '',
            ongoing_status: '',
            countdown: 0,
            showAlert: false,
            test: ''
        }
        this._onpress_task_profile = this._onpress_task_profile.bind(this)
    }
    showAlert = () => {
        this.setState({
            showAlert: true,
        });
    };
    hideAlert = () => {
        this.setState({
            showAlert: false,
        });
    };
    componentDidMount() {
        const { result } = this.props;
        this.setState({  result: result, task_data: result.tasks })
    }
    _onpress_task_profile(result, item, state) {
        this.props.navigation.navigate('Task_profile', { result: result, item: item, state: state })
    }
    _onpress_user_profile(item) {
        this.props.navigation.navigate('User_profile', { user_item: item, result: this.state.result })
    }
    _onpress_time_on(task_data) {
        //this.state.task_data=task_data
    }
    _onpress_task_listing() {
        this._update_data(1);
    }
    _onpress_log_listing() {
        this._update_data(4);
    }
    _onpress_user_listing() {
        this._update_data(3);
    }
    _onpress_working_list() {
        this._update_data(2);
    }
    _update_data(number) {
        const { result } = this.props;
        switch (number) {
            case 1: this.props.navigation.navigate('Task_list', { result: result, task_data: result.tasks }); break;
            case 2: this.props.navigation.navigate('Currently_list', { result: result, task_data: result.tasks }); break;
            case 3: this.props.navigation.navigate('User_list', { result: result }); break;
            case 4: this.props.navigation.navigate('Logs_list', { result: result }); break;
        }
    }
    _onpress_logout() {
        this.props.navigation.navigate('Login', { flag: true })
    }
    select_header() {
        return <View style={styles.upt_right}>
            <Text style={styles.head_title}>DASHBOARD</Text>
        </View>
    }
    select_body() {
        let { result } = this.props;
        return <View>
            <View style={styles.user_image} >
                <View style={styles.welcomeImage} >
                    <Text style={styles.text_surname}>{result.data.firstname.charAt(0) + result.data.lastname.charAt(0)}</Text>
                </View>
            </View>
            <View style={styles.textcontainer}>
                <Text style={styles.title_username}>{result.data.firstname + " " + result.data.lastname}</Text>
            </View>
            <View style={styles.textcontainer}>
                <Text style={styles.title_deparment}>{result.data.department}</Text>
            </View>
            <TouchableOpacity borderWidth={2} style={styles.category_btnContainer1}
                onPress={() => this._onpress_task_listing()}>
                <Text style={styles.text}>Task listing</Text>
            </TouchableOpacity>
            <TouchableOpacity borderWidth={2} style={styles.category_btnContainer3}
                onPress={() => this._onpress_user_listing()}>
                <Text style={styles.text}>User listing</Text>
            </TouchableOpacity>
            <TouchableOpacity borderWidth={2} style={styles.category_btnContainer2}
                onPress={() => this._onpress_log_listing()}>
                <Text style={styles.text}>Logs listing</Text>
            </TouchableOpacity>
            <TouchableOpacity borderWidth={2} style={styles.category_btnContainer5}
                onPress={() => this._onpress_working_list()}>
                <Text style={styles.text}>Currently Working On</Text>
            </TouchableOpacity>
        </View>

    }
    select_tail() {
        return <View style={styles.button_container}>
            <TouchableOpacity style={styles.btnContainer}
                onPress={() => this._onpress_logout()}>
                <Text style={styles.text_logout}>Logout</Text>
            </TouchableOpacity>
        </View>
    }
    _onpress_dashboard() {
        this._update_data();
    }
    render() {
        const { result } = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.up_stair}>
                    <View style={styles.up_title}>
                        <View style={styles.up_title}>
                            <View style={styles.upt_left}>
                                <Image source={require('../assets/images/logo.jpg')} style={{ resizeMode: 'stretch', flex: 1 }} />
                            </View>
                            {this.select_header()}
                        </View>
                    </View>
                    {this.select_body()}
                </View>
                <View style={styles.down_stair}>
                    <View style={styles.line} />
                    {this.select_tail()}
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    welcomeImage: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 3,
        marginBottom: 3,
        backgroundColor: 'rgba(18,167,157,1)',
        width: (Dimensions.get("window").width - 120) / 2,
        height: (Dimensions.get("window").width - 120) / 2,
        borderRadius: 100,
        borderWidth: 2,
        borderColor: 'black',
    },
    user_image: {
        width: Dimensions.get("window").width - 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    up_title: {
        marginTop: 10,
        width: Dimensions.get("window").width - 50,
        marginBottom: 5,
        flexDirection: 'row',
    },
    upt_left: {
        flex: 1,
        height: 70,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(35,31,32,1)',
        marginRight: 10
    },
    upt_right: {
        flex: 3,
        height: 70,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(87,88,90,1)',
    },
    container: {
        flexDirection: 'column',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(209,210,212,1)',
        height: Dimensions.get("window").height,
    },
    up_stair: {
        flex: 10,
        marginBottom: 10,
    },
    down_stair: {
        marginTop: 10,
        flex: 2
    },
    textcontainer: {
        marginTop: 2,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: Dimensions.get("window").width - 50,
    },
    button_container: {
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',

        width: Dimensions.get("window").width - 50,

    },
    title: {
        alignItems: "center",
        fontSize: 22,
        color: 'black'
    },
    title_username: {
        alignItems: "center",
        fontSize: 22,
        color: 'black'
    },
    text_surname: {
        alignItems: "center",
        fontSize: 56,
        color: 'black',
        fontWeight: 'bold',
    },
    title_deparment: {
        alignItems: "center",
        fontSize: 24,
        color: 'black',
        fontWeight: 'bold',
    },
    head_title: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'white',
        fontSize: 24,
    },
    btnContainer: {
        marginTop: 5,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(255,2,2,0.8)',
        height: 39,
        borderRadius: 5,
        zIndex: 100,
        width: 90
    },
    category_btnContainer1: {
        marginTop: 10, alignItems: 'flex-start', justifyContent: 'center', width: Dimensions.get("window").width - 50,
        backgroundColor: 'rgba(16,116,188,1)',
        height: 39, borderRadius: 5, zIndex: 100, borderWidth: 1, borderColor: 'black'
    },
    category_btnContainer2: {
        marginTop: 10, alignItems: 'flex-start', justifyContent: 'center', width: Dimensions.get("window").width - 50,
        backgroundColor: 'rgba(18,167,157,1)',
        height: 39, borderRadius: 5, zIndex: 100, borderWidth: 1, borderColor: 'black'
    },
    category_btnContainer3: {
        marginTop: 10, alignItems: 'flex-start', justifyContent: 'center', width: Dimensions.get("window").width - 50,
        backgroundColor: 'rgba(241,98,59,1)',
        height: 39, borderRadius: 5, zIndex: 100, borderWidth: 1, borderColor: 'black'
    },
    category_btnContainer4: {
        marginTop: 10, alignItems: 'flex-start', justifyContent: 'center', width: Dimensions.get("window").width - 50,
        backgroundColor: 'rgba(238,29,35,1)',
        height: 39, borderRadius: 5, zIndex: 100, borderWidth: 1, borderColor: 'black'
    },
    category_btnContainer5: {
        marginTop: 10, alignItems: 'flex-start', justifyContent: 'center', width: Dimensions.get("window").width - 50,
        backgroundColor: 'rgba(13,147,68,1)',
        height: 39, borderRadius: 5, zIndex: 100, borderWidth: 1, borderColor: 'black'
    },
    text: {
        marginLeft: 10,
        color: 'white',
        fontSize: 20,
    },
    line: {
        marginTop: 20,
        borderColor: 'black',
        borderWidth: 1,
        width: Dimensions.get("window").width - 50,
        height: 2,
        backgroundColor: 'rgba(0,0,0,1)',
    },
    text_logout: {
        fontSize: 24,
        color: 'white',
    }
});

const mapStateToProps = state => ({
    result: state.session.res
})

export default connect(mapStateToProps)(dashboard);

