import React, { Component } from 'react';
import {
    Dimensions,
    Image,
    View,
    StyleSheet,
    TouchableOpacity,
    Text,
    ScrollView,
    AppState,
    BackHandler
} from 'react-native';
import Timer_view from './timer'
import * as constant from "./constant";
import './util/constant'
import { connect } from 'react-redux'
import { updatetask } from '../actions/actioncreators/selectTask'

class tasklisting extends Component {
    static navigationOptions = {
        title: null,
        header: null,
    };
    constructor(props) {
        super(props);
        const {result} = this.props;
        const task_data = result.tasks;
        this.state = {
            user_information: '',
            Username: '',
            password: '',
            tableHead: ['13/12/18', '1:20:00', '2'],
            result: result,
            task_data: task_data,
            countdown: 0,
            count_sec_Right: 0,
            appstate: AppState.currentState,
            testFlag: false,
        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        setInterval(() => {
            this.setState({ time: new Date().toLocaleTimeString() })
        }, 1000)
        this._onpress_task_profile = this._onpress_task_profile.bind(this)
    }
    componentDidUpdate(prevProps) {
        if (prevProps.result !== this.props.result) {
            const { result } = this.props;
            this.setState({ result: result, task_data: result.tasks })
        }
    }
      handleBackButtonClick() {
        this.props.navigation.goBack(null);
        const { result } = this.props;
        this.setState({ result: result, task_data: result.tasks, testFlag: true })
        return true;
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
   
    async   _update_data() {
        const { result } = this.props;
        try {
            fetch(`${constant.domain}/update_data/`, {
                method: 'POST',
                header: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    username: result.data.name,
                    password: result.data.password,
                })
            })
                .then((response) => response.json())
                .then((res => {
                    this.setState({ task_data: res.tasks })
                    this.setState({ result: res })
                })
                ).done();
        }
        catch (e) {
        }
    }
    _onpress_dashboard() {
        this._update_data()
        this.props.navigation.navigate('Dash')
    }
    _onpress_working_list() {
        const { result } = this.props;
        this.props.navigation.navigate('Currently_list', { result: result, task_data: result.tasks})
    }
    async _onpress_task_profile(result, item, state) {
        await this.props.updatetask(item);
        this.props.navigation.navigate('Task_profile', { result: result, item: item, state: state })
    }

    _onpress_time_on(task_data) {
    }
    render() {
        let { result } = this.props;
        let  task_data  = result.tasks;
        return (
            <View style={styles.container}>
                <View style={styles.up_stair}>
                    <View style={styles.up_title}>
                        <View style={styles.up_title}>
                            <View style={styles.upt_left}>
                                <Image source={require('../assets/images/logo.jpg')} style={{ resizeMode: 'stretch', flex: 1 }} />
                            </View>
                            <View style={styles.upt_right}>
                                <Text style={styles.head_title}>Task Listing</Text>
                            </View>
                        </View>
                    </View>
                    <ScrollView>
                        {task_data.map((item, index) => (
                            <View>
                                <Timer_view
                                    result={result}
                                    item={item}
                                    name={item.name}
                                    end_date={item.end_date}
                                    spent_time={item.spent_time}
                                    users_number={item.users_number}
                                    status={item.status}
                                    ongoing_status={item.ongoing_status}
                                    start_time={item.start_time}
                                    priority={item.priority}
                                    username={result.data.name}
                                    password={result.data.password}
                                    task_id={item.id}
                                    _onpress_task_profile={this._onpress_task_profile}
                                    _ontimeer={this._onpress_time_on}
                                    testFlag={this.state.testFlag}
                                />
                            </View>

                        ))
                        }
                    </ScrollView>
                </View>
                <View style={styles.down_stair}>
                    <View style={styles.line} />
                    <View style={styles.button_container}>

                        <TouchableOpacity style={styles.btnContainer1}
                            onPress={() => this._onpress_working_list()}>

                            <Text style={styles.text_logout}>WORKING</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btnContainer2}
                            onPress={() => this._onpress_dashboard()}>
                            <Text style={styles.text_logout}>DASHBOARD</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    up_title: {
        marginTop: 10,
        width: Dimensions.get("window").width - 50,
        marginBottom: 5,
        flexDirection: 'row',
    },
    upt_left: {
        flex: 1,
        height: 70,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(35,31,32,1)',
        marginRight: 10
    },
    upt_right: {
        flex: 3,
        height: 70,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(87,88,90,1)',
    },

    container: {
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: 'rgba(209,210,212,1)',
        height: Dimensions.get("window").height,

    },
    up_stair: {
        flex: 10,
        marginBottom: 10,

    },
    down_stair: {
        marginTop: 10,
        flex: 2

    },
    textcontainer: {
        marginTop: 2,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: Dimensions.get("window").width - 50,

    },
    button_container: {
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',

        width: Dimensions.get("window").width - 50,

    },
    title: {
        alignItems: "center",
        fontSize: 22,
        color: 'black'
    },
    title_username: {
        alignItems: "center",
        fontSize: 22,
        color: 'black'
    },
    title_deparment: {
        alignItems: "center",
        fontSize: 24,
        color: 'black',
        fontWeight: 'bold',
    },
    head_title: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'white',
        fontSize: 24,

    },

    container_upstair: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',



    },
    container_downstair: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'flex-start',




    },



    btnContainer: {
        marginTop: 5,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(255,2,2,0.8)',
        height: 39,
        borderRadius: 5,
        zIndex: 100,
        width: 90
    },




    text: {
        marginLeft: 10,
        color: 'white',
        fontSize: 20,

    },
    text_down: {
        marginTop: 10,
        color: 'black',
        fontSize: 20,
        marginBottom: 10,

    },
    line: {
        marginTop: 20,
        borderColor: 'black',
        borderWidth: 1,
        width: Dimensions.get("window").width - 50,
        height: 2,
        backgroundColor: 'rgba(0,0,0,1)',

    },
    //----------------------- //
    user_big_cell: {
        flexDirection: "row",
        width: Dimensions.get("window").width - 50,
        marginBottom: 3,

    },
    user_second_Lfcell: {
        flexDirection: "column",
        flex: 10,
        marginRight: 5,
        height: 46,
        justifyContent: 'center',
        borderRadius: 5,


    },
    user_second_Rtcell: {
        flex: 2,
        marginLeft: 5,
        height: 42,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,

    },
    user_third_Lf_downcell: {
        flex: 1,
        marginTop: 5,

        flexDirection: "row"
    },
    user_third_Lf_upcell: {
        flex: 1,
        marginBottom: 5,
        backgroundColor: 'rgba(128,130,133,1)',
        borderRadius: 5,
    },
    text_users_Tasks: {
        marginLeft: 0,
        color: 'black',
        fontSize: 20,

    },
    number_Tasks: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'black',
        fontSize: 16,

    },
    lf_upcell: {
        marginLeft: 10,
        marginTop: 10,
        color: 'white',
        fontSize: 20,
        justifyContent: 'center',


    },
    btnContainer1: {
        marginTop: 10,
        marginLeft: 4,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(13,147,68,1)',
        height: 39,
        borderRadius: 5,
        zIndex: 100,
        width: 110,
    },
    btnContainer2: {
        marginTop: 10,
        marginLeft: 4,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(87,88,90,1)',
        height: 39,
        borderRadius: 5,
        zIndex: 100,
        width: 110,
    },
    text_logout: {
        marginLeft: 0,
        color: 'white',
        fontSize: 16,

    },
    ///------------------------------------------------------------
    log_big_cell: {
        flexDirection: "column",
        width: Dimensions.get("window").width - 50,
        //height: 50,
        marginBottom: 15,

    },
    time_log_text: {
        width: Dimensions.get("window").width - 50,
        marginBottom: 10,
        height: 20,

    },
    log_second_Lfcell: {
        flexDirection: "column",
        flex: 8,
        marginRight: 5,

    },
    log_second_upcell: {
        backgroundColor: 'rgba(128,130,133,1)',
        width: Dimensions.get("window").width - 50,
        height: 40,
        borderRadius: 5,


    },
    log_second_downcell: {
        flexDirection: "row",
        marginTop: 5,
        width: Dimensions.get("window").width - 50,
        height: 40
    },
    content_text: {
        // marginLeft: 10,
        marginTop: 10,
        color: 'black',
        fontSize: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
const mapStateToProps = state => ({
    result: state.session.res

})
const mapDispatchToProps = {
    updatetask: updatetask
}

export default connect(mapStateToProps, mapDispatchToProps)(tasklisting);