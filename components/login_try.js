import React, { Component } from 'react';

import {
    Dimensions, AsyncStorage, BackHandler, BackAndroid,
    Animated, Easing, ImageBackground, Image, WebView,
    View, StyleSheet, TextInput, TouchableOpacity, Text, KeyboardAvoidingView,Alert
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Progress from 'react-native-progressbar';
import SQLite from 'react-native-sqlite-2';
const db = SQLite.openDatabase('db.db');
import * as constant from "./constant";
import {observer} from 'mobx-react';
import TaskData from '../src/Store'
export default class mylogin extends Component {
    static navigationOptions = {
        title: null,
        header: null,

    };
    constructor(props) {
        super(props)
        this.state = {

            company: '',
            Username: '',
            password: '',
            flag: true,
            result:''
        }
    }
    componentDidMount() {

        db.transaction(tx => {
            tx.executeSql(
                'create table if not exists items (id integer primary key not null, done int, value text);'
            );
        });
        db.transaction(tx => {
            tx.executeSql(
                `select * from items where done = ?;`,
                [this.props.done ? 1 : 0],
                (_, { rows: { _array } }) => { this._detect_login(_array); }
            );
        });


    }
    _detect_login(user_information) {
        const nNumber = user_information.length

        if (nNumber > 0) {
            user_information.map(({ id, done, value }) => {
                switch (id) {
                    case nNumber - 2: this.setState({ company: value }); break;
                    case nNumber - 1: this.setState({ Username: value }); break;
                    case nNumber: this.setState({ password: value }); break;
                    default: break;
                }
            }
            )
        }

    }
    add(text) {
        db.transaction(
            tx => {
                tx.executeSql('insert into items (done, value) values (0, ?)', [text]);
                tx.executeSql('select * from items', [], (_, { rows }) =>
                    console.log(JSON.stringify(rows))
                );
            },
            null,
            //   this.update
        );
    }
    _storeData = async (res) => {
        try {
            await AsyncStorage.setItem('result_data', res);
        } catch (error) {
            // Error saving data
        }
    }

    _onpress_login() {
        this.refs.progress.show();
        this.add(this.state.company);
        this.add(this.state.Username);
        this.add(this.state.password);
        try {
            fetch(`${constant.domain}/login/`, {
                method: 'POST',
                header: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    username: this.state.Username,
                    password: this.state.password,
                    company:this.state.company
                })
            })
                .then((response) => response.json())
                .then((res => {

                    if (res.status == 1 ) {
                        TaskData.update(res);
                        if (res.priv == 'admin') {

                            this.refs.progress.finish();
                            this.props.navigation.navigate('Dash', { result: res, window_status: 0 })
                           
                        }
                        else {
                            this.refs.progress.finish();

                            this.props.navigation.navigate('User_dash', { result: res, window_status: 0 })
                        }

                    }
                    else {
                        this.refs.progress.finish();
                        Alert.alert(
                            '',
                            'Please input your information correctly',
                            [
                                { text: 'OK', onPress: () => console.log('OK Pressed') },
                            ],
                            { cancelable: false }
                        )


                    }
                })
                ).done();
        }
        catch (e) {
            this.refs.progress.finish();

        }

        //
        //this.props.navigation.navigate('User_profile')




    }
    _onpress_exit() {
        BackAndroid.exitApp();
    }
    _onpress_back() {
        this.props.navigation.navigate('Dash',{result:this.state.result,window_status: 0});
    }
    _select_part() {
        if (this.state.flag) {
            return<KeyboardAwareScrollView>
                <View style={styles.container}>

                    <Image source={require('../assets/logo.jpg')} style=

                        {styles.welcomeImage} />
                    <View style={styles.textcontainer}>
                        <Text style={styles.title}>Company</Text>
                    </View>
                    <TextInput style={styles.inContainer_username}
                        underlineColorAndroid="transparent"
                        onChangeText={(company) => this.setState({
                            company

                        })}
                        value={this.state.company}
                    />
                    <View style={styles.textcontainer}>
                        <Text style={styles.title}>Username</Text>
                    </View>
                    <TextInput style={styles.inContainer_username}
                        underlineColorAndroid="transparent"
                        onChangeText={(Username) => this.setState({

                            Username
                        })}
                        value={this.state.Username}
                    />
                    <View style={styles.textcontainer}>
                        <Text style={styles.title}>Password</Text>
                    </View>
                    <TextInput style={styles.inContainer_username}
                        secureTextEntry={true}
                        underlineColorAndroid="transparent"
                        onChangeText={(password) => this.setState({

                            password
                        })}
                        value={this.state.password}
                    />
                    <Progress ref={'progress'} title={'Connecting...'} />
                    <View style={styles.button_container}>

                        <TouchableOpacity style={styles.btnContainer}
                            onPress={() => this._onpress_login()}>

                            <Text style={styles.text}>Login</Text>
                        </TouchableOpacity>
                    </View>

                    <Text style={styles.text_down}>Copyright@2018</Text>




                </View>
            </KeyboardAwareScrollView>
        }
        else{
            return  <View style={styles.alert_container} borderWidth={1} borderColor={'black'}>
            <View marginTop={20} marginBottom={15}>
                <Text style={styles.title}>Do you want to exit ?</Text>
            </View>
            <View style={styles.btn_container}>
                <TouchableOpacity style={styles.alert_btnContainer}
                    onPress={() => this._onpress_exit()}>

                    <Text style={styles.text}>Yes</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.alert_btnContainer}
                    onPress={() => this._onpress_back()}>

                    <Text style={styles.text}>No</Text>
                </TouchableOpacity>
            </View>
        </View>
        }
    }



    render() {





        return (
            <View style={styles.container}>
              {this._select_part()}

            </View>


        );
    }
}
const styles = StyleSheet.create({
    welcomeImage: {
        marginTop: 45,
        marginBottom: 30,
        width: Dimensions.get("window").width - 90,
        height: (Dimensions.get("window").width - 90) / 2
    },

    container: {
        flexDirection: 'column',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(209,210,212,1)',
        height: Dimensions.get("window").height,

    },
    btn_container: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',



    },
    alert_container: {
        flexDirection: 'column',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(255,255,255,1)',
        marginTop: Dimensions.get("window").height / 2 - 80,
        height: 130,
        width: Dimensions.get("window").width - 100,
        borderRadius: 30,
    },
    textcontainer: {
        marginTop: 10,
        flexDirection: 'row',

        width: Dimensions.get("window").width - 90,

    },
    button_container: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',

        width: Dimensions.get("window").width - 90,

    },
    title: {
        alignItems: "flex-start",
        fontSize: 24,
        color: 'black'
    },

    container_upstair: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',



    },
    container_downstair: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'flex-start',




    },

    inContainer_username: {
        // alignItems: 'center',
        height: 42, width: Dimensions.get("window").width - 90, borderColor: 'rgba(0,0,0,1)', borderWidth: 1,
        marginTop: 5,
        fontSize: 20,
        backgroundColor: 'rgba(255,255,255,1)',
        borderRadius: 5,
        textAlign: 'center',
        //fontFamily: 'myanmar',
        color: 'black',
    },
    inContainer_password: {
        // alignItems: 'center',
        height: 36, width: Dimensions.get("window").width - 90, borderColor: 'rgba(153,153,153,1)', borderWidth: 1,
        marginTop: 30,

        backgroundColor: 'rgba(153,153,153,1)',

        textAlign: 'center',
        //fontFamily: 'myanmar',
        color: 'black',
    },
    btnContainer: {
        marginTop: 15,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,166,220,0.8)',
        height: 39,
        borderRadius: 5,
        zIndex: 100,
        width: 90,
        borderColor: 'rgba(0,0,0,1)'
    },
    alert_btnContainer: {
        marginTop: 15,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,166,220,0.8)',
        height: 25,
        borderRadius: 5,
        zIndex: 100,
        width: 75,
        marginLeft: 15,
        marginRight: 15,
        borderColor: 'rgba(0,0,0,1)'

    },

    text: {
        color: 'white',
        fontSize: 16,

    },
    text_down: {
        marginTop: 40,
        color: 'black',
        fontSize: 20,
        marginBottom: 10,

    },
    text_head: {
        marginTop: 20,
        color: 'white',
        fontSize: 25,
        marginBottom: 10
    },

});
