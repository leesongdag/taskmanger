import { createStackNavigator } from 'react-navigation';
import loginpage from './login';
import login_trypage from './login_try';
import dashboardpage from './dashboard';
import tasklistingpage from './Task_Listing'
import userlistingpage from './user_listing'
import logslistingpage from './logs_listing'
import taskprofilepage from './profile/taskprofile'
import user_taskprofilepage from './user/user_taskprofile'
import userprofilepage from './profile/userprofile'
import currentworkingpage from './currently_workingon'
import workinglistpage from './working_listing'
import completedpage from './user_information_detail/completed_tasks'
import ongoingtaskpage from './user_information_detail/ongoing_tasks'
import pendingtaskpage from './user_information_detail/tasks_pending'
import user_userprofilepage from './user/user_userprofile'
import userlogpase from './user_information_detail/user_logs'
import userdashboardpage from './user/user_dashboard'
import  user_currently_working from './user/user_currently_workingon'
import usertasklistingpage from './user/user_task_listing'
import user_log_listing from './user/user_logs_listing'
import timerpage from './timer'

import user_completed_listing from './user/user_completed_tasks'
const AppNavigator = createStackNavigator({
  Home: { screen: loginpage },
  Login:{screen:login_trypage},
  Dash: { screen: dashboardpage},
  Task_list:{screen:tasklistingpage},
  User_list:{screen:userlistingpage},
  Logs_list:{screen:logslistingpage},
  Currently_list:{screen:currentworkingpage},
  Working_list:{screen:currentworkingpage},
  Task_profile:{screen:taskprofilepage},
  User_profile:{screen:userprofilepage},
  User_userprofile:{screen:user_userprofilepage},
  Complet_task:{screen:completedpage},
  Ongoing_task:{screen:ongoingtaskpage},
  Pending_task:{screen:pendingtaskpage},
  Time_page:{screen:timerpage},
  // ----------------------------------------------//
  User_log:{screen:userlogpase},
  User_dash:{screen:userdashboardpage},
  User_task_listing:{screen:usertasklistingpage},
  user_taskprofilepage:{screen:user_taskprofilepage},
  user_Currently_list:{screen:user_currently_working},
  user_log_listing:{screen:user_log_listing},
  user_completed_listing:{screen:user_completed_listing,}

});

export default AppNavigator;