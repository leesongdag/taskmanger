import React, { Component } from 'react';
import {Dimensions, TouchableOpacity,Switch, Button, StyleSheet, Text, View, TextInput } from 'react-native';
import PropTypes from "prop-types";
import { Table, Row, Rows } from 'react-native-table-component';
import Progress from 'react-native-progressbar';
import Moment from 'moment';
import * as constant from "../constant";
export default class profiletimer extends React.Component {
    static navigationOptions = {
        title: null,
        header: null,

    };
    static propTypes = {
        name: PropTypes.string,
        end_date:PropTypes.string,
        spent_time:PropTypes.string,
        users_number: PropTypes.string,
        status:PropTypes.string,
        priority:PropTypes.string,
        username:PropTypes.string,
        password:PropTypes.string,
        task_id:PropTypes.string,
        ongoing_status:PropTypes.string,
        start_time:PropTypes.string,
        result:PropTypes.object,
        item:PropTypes.object,
        _onpress_task_profile:PropTypes.func,
        _ontimeer:PropTypes.func
    
    }
  constructor(props) {
     // 
    super(props)
    this.state = {
        result:'',
        start:'',
        ongoing_status:'',
      
      sec_Right:this.props.spent_time.charAt(8),
      sec_Left: this.props.spent_time.charAt(7),//time_data[1],
      min_Right: this.props.spent_time.charAt(5),
      min_Left: this.props.spent_time.charAt(4),
      hour_Right: this.props.spent_time.charAt(2),
      hour_Left: this.props.spent_time.charAt(1),
      intervalReference: null,
      originalCountdown: 0,
      countdown: 0,
      toggle: true,
      noty_string: '',
      flag:false,          
      spent_time:this.props.spent_time,       
      time: new Date().toLocaleTimeString(),
      countdown: 0,
      count_sec_Right:Number(this.props.spent_time.charAt(7)),
      count_sec_Left:Number(this.props.spent_time.charAt(6)),
      count_min_Left:Number(this.props.spent_time.charAt(3)),//Math.floor(Number(boat_data.working_minute)/10),
      count_min_Right:Number(this.props.spent_time.charAt(4)),//Number(boat_data.working_minute)-Math.floor(Number(boat_data.working_minute)/10)*10,
      count_hour_Right:Number(this.props.spent_time.charAt(1)),//Number(boat_data.working_hour)-Math.floor(Number(boat_data.working_hour)/10)*10,
      count_hour_Left:Number(this.props.spent_time.charAt(0)),//Math.floor(Number(boat_data.working_hour)/10),
      toggle: true,
      }

    setInterval(() => {
      this.setState({ time: new Date().toLocaleTimeString() })
    }, 1000)
    this.startCountDown = this.startCountDown.bind(this)
    this.pause = this.pause.bind(this)
    this.resume = this.resume.bind(this)
    this.reset = this.reset.bind(this)
    this.toggle = this.toggle.bind(this)


  }
  //

  pause() {
    clearInterval(this.state.intervalReference)
  }
  resume() {
    // console.log(this.state)
    this.startCountDown(this.state.countdown)
  }
  reset() {
    this.pause();
    this.setState({ sec_Right: 0, sec_Left: 0, min_Right: 0, min_Left: 0, hour_Right: 0 })


  }

  toggle() {
    this.setState({ toggle: !this.state.toggle })
  }
  
  _onpress_profile() {
      this.props._onpress_task_profile(this.props.result,this.props.item);
//const result = this.props.navigation.getParam('result');
//alert(this.props.result.status)
   // this.props.navigation.navigate('Task_profile', { result: this.props.result, item: this.props.item })
}


  componentWillMount() {
            this.setState({ongoing_status:this.props.ongoing_status});
      if(this.props.ongoing_status=="1") {
     
      this.setState({start:this.props.start_time})
      this.startCountDown();
    }
}
   
  render() {
      let{sec_Left}=this.state;
      let{sec_Right}=this.state;
      let{min_Left}=this.state;
      let{min_Right}=this.state;
    
      let{hour_Left}=this.state;
      let{hour_Right}=this.state;
    
    return (
        <View style={styles.big_cell}>
   <Progress ref={'progress'} title={'Connecting...'}/>
                                <View style={styles.second_Lfcell}>
                                    <View style={styles.third_Lf_upcell} borderRadius={4}>
                                            <Text style={styles.lf_upcell}>{this.props.name}</Text>
                                        
                                    </View>
                                    <View style={styles.third_Lf_downcell}>
                                        <View flex={9} >
                                            <Table borderStyle={{ borderWidth: 2, borderColor: '#000' }}>
                                                <Row data={[this.props.end_date,this.state.spent_time, this.props.users_number]}  textStyle={styles.table_text} />
                                            </Table>
                                        </View>
                                        <View flex={2} marginLeft={3} fontSize={22} justifyContent={'center'} alignItems={'center'} backgroundColor={this.props.status == "Q" ? 'rgba(247,147,29,1)' : 'rgba(0,165,79,1)'}>
                                            <Text style={styles.text_status_priority}>{this.props.status}</Text>
                                        </View>
                                        <View flex={2} marginLeft={3} fontSize={22} justifyContent={'center'} alignItems={'center'} backgroundColor='rgba(238,29,35,1)'>
                                            <Text style={styles.text_status_priority}>{this.props.priority}</Text>
                                        </View>

                                    </View>

                                </View>

                                {/*-----------------------------------------*/}

                                <View style={styles.second_Rtcell}>
                                    <View flex={1} backgroundColor={this.state.ongoing_status == "0" ? 'rgba(128,130,133,1)' : 'rgba(13,147,68,1)'} fontSize={22} justifyContent={'center'} alignItems={'center'}>
                                        <TouchableOpacity
                                            onPress={() => this._onpress_ON()}>

                                            <Text style={styles.text_status_priority}>ON</Text>
                                        </TouchableOpacity>

                                    </View>
                                    <View flex={1} backgroundColor={this.state.ongoing_status == "0" ? 'rgba(238,29,35,1)' : 'rgba(128,130,133,1)'} fontSize={22} justifyContent={'center'} alignItems={'center'}>
                                       <TouchableOpacity
                                            onPress={() => this._onpress_off()}>
                                     
                                            <Text style={styles.text_status_priority}>OFF</Text>
                                       </TouchableOpacity>

                                    </View>
                                  
                                </View>
                               
                      
      </View>
    );
  }
  _onpress_off() {
      this.pause();
      this.setState({ongoing_status:'0'})
    this.refs.progress.show();
    const current_date = new Date();
    //console.log(time)
    try{  fetch(`${constant.domain}/add_time_log/`, {
        method: 'POST',
        header: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            username:this.props.username,
            password:this.props.password,
            task_id :this.props.task_id,
            date:current_date,
            work_status:this.props.status,
            time:this.state.spent_time
        })
      })
        .then((response) => response.json())
        .then((res => {
            this.refs.progress.finish();
      
            this.setState({result:res});
          //  alert(res.status_message)
          //  this.setState({Task_list_data:res.tasks});
      
           this.render()
        
        })
        ).done();}
        catch (e) {
            this.refs.progress.finish();
         
        
    }



}


  _onpress_ON(){

    if( this.state.ongoing_status=="0"){
        this.setState({ongoing_status:1});
    const current_date = new Date();
    const time = current_date.toLocaleTimeString();
  
   
   console.log(time)
   this.setState({start:time});
   this.refs.progress.show();
  
    try{  fetch(`${constant.domain}/update_start_time/`, {
        method: 'POST',
        header: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            username:this.props.username,
            password:this.props.password,
            task_id :this.props.task_id,
            task_start_time:time,
            work_status:this.props.status,
            date:current_date,
        })
      })
        .then((response) => response.json())
        .then((res => {
            this.refs.progress.finish();
            this.props._ontimeer(res.tasks);
           this.setState({result:res});
            this.render()
        
        })
        ).done();}
        catch (e) {
            this.refs.progress.finish();
         
        
    }
   

    this.startCountDown();
    }


  }

 async startCountDown(countdownTime=this.state.countdown) {
    clearInterval(this.state.intervalReference)
    this.setState({countdown: countdownTime, originalCountdown: countdownTime}, ()=>{
      let intervalReference = setInterval(()=>{
          if(this.state.count_sec_Right<10)
                 this.setState({countdown: ++this.state.count_sec_Right})
           const now=new Date().toLocaleTimeString();
           const a=Moment(now,("HH:mm:ss"));//;
           const b=Moment(this.state.start,("HH:mm:ss"))
            const  mins =Moment.utc(Moment(now,"HH:mm:ss").diff(Moment(this.state.start,"HH:mm:ss"))).format("HH:mm:ss")
            //const  second =Moment.utc(Moment(now,"HH:mm:ss").diff(Moment(this.state.start,"HH:mm:ss"))).format("ss")
        
          //  const hours=Moment.utc(b.diff(a,'hours'))
          
             this.setState({spent_time:mins});
     

      }, 1000)
      
      this.setState({intervalReference: intervalReference})
    })
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',

  },
  containertext: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textcontainer: {
    marginTop: 3,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: Dimensions.get("window").width - 50,

},
button_container: {
    marginTop: 5,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',

    width: Dimensions.get("window").width - 50,

},
title: {
    alignItems: "center",
    fontSize: 22,
},

container_upstair: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',



},
container_downstair: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'flex-start',

},



btnContainer1: {
    marginTop: 10,
    marginLeft: 4,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(13,147,68,1)',
    height: 39,
    borderRadius: 5,
    zIndex: 100,
    width: 110,
},
btnContainer2: {
    marginTop: 10,
    marginLeft: 4,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(87,88,90,1)',
    height: 39,
    borderRadius: 5,
    zIndex: 100,
    width: 110,
},


text: {
    marginLeft: 10,
    color: 'white',
    fontSize: 16,

},
lf_upcell: {
    marginLeft: 10,
    marginTop: 10,
    color: 'white',
    fontSize: 20,
    justifyContent: 'center',


},
table_text: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    color: 'black',
    fontSize: 14,
    marginTop:10,
    marginBottom:10,

},
head_title: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    color: 'white',
    fontSize: 24,

},
text_logout: {
    marginLeft: 0,
    color: 'white',
    fontSize: 16,

},
text_status_priority: {
    marginLeft: 0,
    color: 'white',
    fontSize: 20,

},

  containertime: {
    flexDirection: 'row',
    marginTop: 3,
    justifyContent: 'space-around',
  },
  big_cell: {
    flexDirection: "row",
    width: Dimensions.get("window").width - 50,
    height: 100,
    marginBottom: 20

},
second_Lfcell: {
    flexDirection: "column",
    flex: 6,
    marginRight: 5,

},
second_Rtcell: {
    flex: 1,
    flexDirection: "column",
    marginLeft: 5,

},
third_Lf_downcell: {
    flex: 1,
    marginTop: 5,
    height:40,

    flexDirection: "row"
},
third_Lf_upcell: {
    flex: 1,
    marginBottom: 5,
    backgroundColor: 'rgba(128,130,133,1)',
}
});
