import React, { Component } from 'react';

import {
    Dimensions,
    Animated, Easing, ImageBackground, Image, WebView,
    View, StyleSheet, TextInput, TouchableOpacity, Text, ScrollView
} from 'react-native';
import { Table, Row, Rows } from 'react-native-table-component';
export default class userlisting extends Component {
    static navigationOptions = {
        title: null,
        header: null,

    };
    state = {
        user_information: '',
        Username: '',
        password: '',
        tableHead: ['13/12/18', '1:20:00', '2'],
    };

    componentWillMount() {

        // this.setState({user_information});
        //  this._detect_login(user_information);
    }
    _onpress_dashboard() {
        this.props.navigation.navigate('Dash')

    }
    _onpress_working_list() {
        const result = this.props.navigation.getParam('result');

        this.props.navigation.navigate('Currently_list', { result: result })
    }



    render() {

        const result = this.props.navigation.getParam('result');
        const time_log_list_data = result.time_log;
        const user_data = this.props.navigation.getParam('item');




        return (

            <View style={styles.container}>
                <View style={styles.up_stair}>

                    <View style={styles.up_title}>
                        <View style={styles.upt_left}>
                            <Image source={require('../../assets/images/logo.jpg')} style={styles.welcomeImage} />
                        </View>
                        <View style={styles.upt_right}>
                            <Text style={styles.head_title}>LOGS</Text>
                        </View>
                    </View>
                    {/*---------------------------------------- */}
                    <View style={styles.time_log_text}>
                        <View style={styles.second_Lfcell}>
                            <Text style={styles.text_users_Tasks}>Time Logs</Text>

                        </View>

                    </View>
                    <ScrollView>
                        {/*-----------------------------------------*/}
                        {time_log_list_data.reverse().map((item, index) => {
                            if (item.user_id == user_data.id) {
                                return <View style={styles.big_cell}>
                                    <View style={styles.second_upcell}>
                                        <Text style={styles.lf_upcell}>{"Job" + item.user_id}</Text>

                                    </View>
                                    <View style={styles.second_downcell} >
                                        <View flex={4} borderWidth={2} marginRight={3} >
                                            <Text style={styles.cell_txt}>{item.date}</Text>

                                        </View>
                                        <View flex={3} marginRight={3} borderWidth={2}>
                                            <Text style={styles.cell_txt}>{item.time}</Text>

                                        </View>
                                        <View flex={2} marginRight={3} borderWidth={2}>
                                            <Text style={styles.cell_txt}>{item.user_name }</Text>

                                        </View>
                                        <View flex={1} justifyContent={'center'} alignItems={'center'} backgroundColor={item.task_status == "Q" ? 'rgba(247,147,29,1)' : 'rgba(0,165,79,1)'}>
                                            <Text style={styles.right_downcell}>{item.task_status}</Text>
                                        </View>

                                    </View>



                                </View>
                            }
                        })}
                        {/*-----------------------------------------*/}
                    </ScrollView>
                </View>
                <View style={styles.down_stair}>
                    <View style={styles.line} />
                    <View style={styles.button_container}>

                        <TouchableOpacity style={styles.btnContainer1}
                            onPress={() => this._onpress_working_list()}>

                            <Text style={styles.text_logout}>WORKING</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btnContainer2}
                            onPress={() => this._onpress_dashboard()}>

                            <Text style={styles.text_logout}>DASHBOARD</Text>
                        </TouchableOpacity>
                    </View>
                </View>






            </View>

        );
    }
}
const styles = StyleSheet.create({
    welcomeImage: {
        marginTop: 5,
        marginBottom: 5,

        width: (Dimensions.get("window").width - 90) / 2,
        height: (Dimensions.get("window").width - 90) / 2,
        borderRadius: 100,
    },
    user_image: {
        width: Dimensions.get("window").width - 50,
        justifyContent: 'center',
        alignItems: 'center',


    },
    head: {

        height: 43
    },
    up_title: {
        marginTop: 10,
        width: Dimensions.get("window").width - 50,
        marginBottom: 30,
        flexDirection: 'row',

    },
    upt_left: {
        flex: 1,
        height: 90,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(35,31,32,1)',
        marginRight: 10



    },
    upt_right: {
        flex: 3,
        height: 90,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(87,88,90,1)',



    },

    container: {
        flexDirection: 'column',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(209,210,212,1)',
        height: Dimensions.get("window").height,

    },
    up_stair: {
        flex: 8,
        marginBottom: 30,
        height: 100,
    },
    down_stair: {
        // marginTop: 10,
        flex: 2

    },
    textcontainer: {
        marginTop: 3,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: Dimensions.get("window").width - 50,

    },
    button_container: {
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',

        width: Dimensions.get("window").width - 50,

    },
    title: {
        alignItems: "center",
        fontSize: 22,
    },

    container_upstair: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',



    },
    container_downstair: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'flex-start',

    },



    btnContainer1: {
        marginTop: 10,
        marginLeft: 4,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(13,147,68,1)',
        height: 39,
        borderRadius: 5,
        zIndex: 100,
        width: 110,
    },
    btnContainer2: {
        marginTop: 10,
        marginLeft: 4,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(87,88,90,1)',
        height: 39,
        borderRadius: 5,
        zIndex: 100,
        width: 110,
    },


    text: {
        marginLeft: 10,
        color: 'white',
        fontSize: 16,

    },
    right_downcell: {
        marginTop: 10,
        color: 'white',
        fontSize: 20,
        justifyContent: 'center',


    },
    lf_upcell: {
        marginLeft: 10,
        marginTop: 10,
        color: 'white',
        fontSize: 20,
        justifyContent: 'center',


    },
    cell_txt: {
        marginLeft: 10,
        marginTop: 10,
        color: 'black',
        fontSize: 20,
        justifyContent: 'center',


    },
    table_text: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'black',
        fontSize: 12,

    },
    number_Tasks: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'black',
        fontSize: 16,

    },
    head_title: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'white',
        fontSize: 24,

    },
    text_logout: {
        marginLeft: 0,
        color: 'white',
        fontSize: 16,

    },
    text_users_Tasks: {
        marginLeft: 0,
        color: 'black',
        fontSize: 20,

    },

    line: {
        marginTop: 5,
        borderColor: 'black',
        borderWidth: 1,
        width: Dimensions.get("window").width - 50,
        height: 2,
        backgroundColor: 'rgba(0,0,0,1)',

    },
    //------------------------//
    second_upcell: {
        backgroundColor: 'rgba(128,130,133,1)',
        width: Dimensions.get("window").width - 50,
        height: 40,
        borderRadius: 5,


    },
    second_downcell: {
        flexDirection: "row",
        marginTop: 5,
        width: Dimensions.get("window").width - 50,
        height: 40
    },
    //----------------------- //
    big_cell: {
        flexDirection: "column",
        width: Dimensions.get("window").width - 50,
        //height: 50,
        marginBottom: 10,

    },
    time_log_text: {
        width: Dimensions.get("window").width - 50,
        marginBottom: 10,
        height: 20,

    },
    second_Lfcell: {
        flexDirection: "column",
        flex: 8,
        marginRight: 5,

    },





});
