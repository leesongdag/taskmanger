import React, { Component } from 'react';

import {
    Dimensions, ScrollView,
    Animated, Easing, ImageBackground, Image, WebView,
    View, StyleSheet, TextInput, TouchableOpacity, Text
} from 'react-native';
import { Table, Row, Rows } from 'react-native-table-component';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import * as constant from "../constant";
import Progress from 'react-native-progressbar';
export default class userlisting extends Component {
    static navigationOptions = {
        title: null,
        header: null,

    };
    constructor(props) {
        super(props);

        const result = this.props.navigation.getParam('result');
        const user = this.props.navigation.getParam('user_item');
        // const task_data = this.props.navigation.getParam('item');

        this.state = {
            select_group: "  Select_Group",
            select_group_id: '',
            user_information: '',
            result: result,
            user_data:user,

        };
    }
  
    _menu = null;

    setMenuRef = ref => {
        this._menu = ref;
    };

    hideMenu(name, id) {
        this.setState({ select_group: name, select_group_id: id })

        this._menu.hide();
    };

    showMenu = () => {
        this._menu.show();
    };

    componentWillMount() {

        // this.setState({user_information});
        //  this._detect_login(user_information);
    }
    _onpress_dashboard() {
        const result = this.props.navigation.getParam('result');
        if (result.priv == "admin")
            this.props.navigation.navigate('Dash')
        else
            this.props.navigation.navigate('User_dash')
    }
    _onpress_completed_tasks() {
        const result = this.props.navigation.getParam('result');
        const item = this.props.navigation.getParam('user_item');

        this.props.navigation.navigate('Complet_task', { result: result, item: item })

    }
    _onpress_ongoing_tasks() {
        const result = this.props.navigation.getParam('result');
        const item = this.props.navigation.getParam('user_item');

        this.props.navigation.navigate('Ongoing_task', { result: result, item: item })

    }
    _onpress_user_logs() {
        const result = this.props.navigation.getParam('result');
        const item = this.props.navigation.getParam('user_item');

        this.props.navigation.navigate('User_log', { result: result, item: item })

    }
  
  
    _onpress_pending_tasks() {
        const result = this.props.navigation.getParam('result');
        const item = this.props.navigation.getParam('user_item');

        this.props.navigation.navigate('Pending_task', { result: result, item: item })

    }
    select_part() {
        const result = this.props.navigation.getParam('result');
        if (result.priv == "admin") {
            return <View>
                <View style={styles.second_downcell} >

                    <View flex={2} justifyContent={'center'} alignItems={'center'} backgroundColor='rgba(238,29,35,1)' marginRight={3} borderRadius={4} borderWidth={2}>
                        <TouchableOpacity
                            onPress={() => this._onpress_completed_tasks()}>

                            <Text style={styles.right_downcell}>Completed Task</Text>
                        </TouchableOpacity>
                    </View>
                    <View flex={2} justifyContent={'center'} alignItems={'center'} backgroundColor='rgba(13,147,68,1)' borderRadius={4} marginRight={3} borderWidth={2}>
                        <TouchableOpacity
                            onPress={() => this._onpress_ongoing_tasks()}>

                            <Text style={styles.right_downcell}>Ongoing Tasks</Text>
                        </TouchableOpacity>

                    </View>


                </View>
                <View style={styles.second_downcell} >

                    <View flex={2} justifyContent={'center'} alignItems={'center'} backgroundColor='rgba(209,210,212,1)' marginRight={3} borderRadius={4} borderWidth={2}>
                        <TouchableOpacity
                            onPress={() => this._onpress_pending_tasks()}>

                            <Text style={styles.right_downcell}> Tasks pending</Text>
                        </TouchableOpacity>

                    </View>
                    <View flex={2} justifyContent={'center'} alignItems={'center'} backgroundColor='rgba(16,116,188,1)' marginRight={3} borderWidth={1} borderRadius={4}>
                        <TouchableOpacity
                            onPress={() => this._onpress_user_logs()}>
                            <Text style={styles.right_downcell}>User Logs</Text>
                        </TouchableOpacity>

                    </View>

                </View>
            </View>
        }
    }



    render() {

        const result = this.props.navigation.getParam('result');
        let {user_data} = this.state;




        return (

            <View style={styles.container}>
                <View style={styles.up_stair}>

                    <View style={styles.up_title}>
                        <View style={styles.upt_left}>
                            <Image source={require('../../assets/images/logo.jpg')} style={styles.welcomeImage} />
                        </View>
                        <View style={styles.upt_right}>
                            <Text style={styles.head_title}>USER PROFILE</Text>
                        </View>
                    </View>

                    {this.select_part()}

                    <View style={styles.line} marginBottom={10} />
                    {/*---------------------------------------- */}
                    <ScrollView>
                        <View style={styles.big_cell}>
                            <Text style={styles.text_users_Tasks}>Username</Text>
                        </View>
                        <View style={styles.second_upcell} borderWidth={2}>
                            <Text style={styles.lf_upcell}>{user_data.username}</Text>

                        </View>

                        {/*-----------------------------------------*/}
                        {/*---------------------------------------- */}
                        <View style={styles.big_cell}>
                            <Text style={styles.text_users_Tasks}>Firstname</Text>
                        </View>
                        <View style={styles.second_upcell} borderWidth={2}>
                            <Text style={styles.lf_upcell}>{user_data.firstname}</Text>

                        </View>

                        {/*-----------------------------------------*/}
                        {/*---------------------------------------- */}
                        <View style={styles.big_cell}>
                            <Text style={styles.text_users_Tasks}>Lastname</Text>
                        </View>
                        <View style={styles.second_upcell} borderWidth={2}>
                            <Text style={styles.lf_upcell}>{user_data.lastname}</Text>

                        </View>

                        {/*-----------------------------------------*/}
                        {/*---------------------------------------- */}
                        <View style={styles.big_cell}>
                            <Text style={styles.text_users_Tasks}>Telephone</Text>
                        </View>
                        <View style={styles.second_upcell} borderWidth={2}>
                            <Text style={styles.lf_upcell}>{user_data.telephone}</Text>

                        </View>

                        {/*-----------------------------------------*/}
                        {/*---------------------------------------- */}
                        <View style={styles.big_cell}>
                            <Text style={styles.text_users_Tasks}>Department</Text>
                        </View>
                        <View style={styles.second_upcell} borderWidth={2}>
                            <Text style={styles.lf_upcell}>{user_data.department}</Text>

                        </View>

                        {/*-----------------------------------------*/}
                        {/*---------------------------------------- */}
                        <View style={styles.big_cell}>
                            <Text style={styles.text_users_Tasks}>Group</Text>
                        </View>
                        {/*-----------------------------------------*/}
                      

                        {/*-----------------------------------------*/}
                        <Progress ref={'progress'} title={'Connecting...'} />

                        {/*-----------------------------------------*/}
                        {user_data.user_group_data.map((item, index) => (

                            <View style={styles.second_downcell} >
                                <View flex={3} borderWidth={2} borderRadius={4} marginRight={3} >
                                    <Text style={styles.lf_upcell}>{item.group_name}</Text>

                                </View>

                               

                            </View>
                        ))}

                        {/*-----------------------------------------*/}
                        {/*-----------------------------------------*/}



                        {/*-----------------------------------------*/}

                        {/*-----------------------------------------*/}

                        {/*-----------------------------------------*/}







                        {/*-----------------------------------------*/}

                    </ScrollView>
                </View>
                <View style={styles.down_stair}>
                    <View style={styles.line} />
                    <View style={styles.button_container}>


                        <TouchableOpacity style={styles.btnContainer2}
                            onPress={() => this._onpress_dashboard()}>

                            <Text style={styles.text_logout}>DASHBOARD</Text>
                        </TouchableOpacity>
                    </View>
                </View>






            </View>

        );
    }
}
const styles = StyleSheet.create({
    welcomeImage: {
        marginTop: 5,
        marginBottom: 5,

        width: (Dimensions.get("window").width - 90) / 2,
        height: (Dimensions.get("window").width - 90) / 2,
        borderRadius: 100,
    },
    user_image: {
        width: Dimensions.get("window").width - 50,
        justifyContent: 'center',
        alignItems: 'center',


    },
    head: {

        height: 43
    },
    up_title: {
        marginTop: 10,
        width: Dimensions.get("window").width - 50,
        marginBottom: 10,
        flexDirection: 'row',

    },
    upt_left: {
        flex: 1,
        height: 90,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(35,31,32,1)',
        marginRight: 10



    },
    upt_right: {
        flex: 3,
        height: 90,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(87,88,90,1)',



    },

    container: {
        flexDirection: 'column',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(209,210,212,1)',
        height: Dimensions.get("window").height,

    },
    up_stair: {
        flex: 8,
        marginBottom: 10,
        height: 100,
    },
    down_stair: {
        marginTop: 0,
        flex: 2

    },
    textcontainer: {
        marginTop: 3,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: Dimensions.get("window").width - 50,

    },
    button_container: {
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',

        width: Dimensions.get("window").width - 50,

    },
    title: {
        alignItems: "center",
        fontSize: 22,
    },

    container_upstair: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',



    },
    container_downstair: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'flex-start',

    },



    btnContainer1: {
        marginTop: 10,
        marginLeft: 4,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(13,147,68,1)',
        height: 39,
        borderRadius: 5,
        zIndex: 100,
        width: 110,
    },
    btnContainer2: {
        marginTop: 10,
        marginLeft: 4,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(87,88,90,1)',
        height: 39,
        borderRadius: 5,
        zIndex: 100,
        width: 110,
    },


    text: {
        marginLeft: 10,
        color: 'white',
        fontSize: 16,

    },
    right_downcell: {
        marginTop: 10,
        color: 'white',
        fontSize: 20,
        justifyContent: 'center',


    },
    lf_upcell: {
        marginLeft: 10,
        marginTop: 10,
        color: 'black',
        fontSize: 20,
        justifyContent: 'center',


    },
    table_text: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'black',
        fontSize: 12,

    },
    number_Tasks: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'black',
        fontSize: 16,

    },
    head_title: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'white',
        fontSize: 24,

    },
    text_logout: {
        marginLeft: 0,
        color: 'white',
        fontSize: 16,

    },
    text_users_Tasks: {
        marginLeft: 0,
        color: 'black',
        fontSize: 20,

    },

    line: {
        marginTop: 20,
        borderColor: 'black',
        borderWidth: 1,
        width: Dimensions.get("window").width - 50,
        height: 2,
        backgroundColor: 'rgba(0,0,0,1)',

    },
    //------------------------//
    second_upcell: {
        //  backgroundColor: 'rgba(128,130,133,1)',
        width: Dimensions.get("window").width - 50,
        height: 40,
        borderRadius: 5,
        marginBottom: 5,


    },
    second_downcell: {
        flexDirection: "row",
        marginTop: 5,
        width: Dimensions.get("window").width - 50,
        height: 40,
        marginBottom: 5
    },
    //----------------------- //
    big_cell: {
        width: Dimensions.get("window").width - 50,
        marginTop: 10,
    },
    big_row_cell: {
        flexDirection: "row",
        width: Dimensions.get("window").width - 50,
        height: 50,

    },
    second_Lfcell: {
        flexDirection: "column",
        flex: 8,
        marginRight: 5,

    },
    //---------comment-------------- //
    big_cell_commment: {
        flexDirection: "column",
        width: Dimensions.get("window").width - 50,
        height: 120,
        borderWidth: 2,
        borderRadius: 10,
        padding: 5
    },
    second_downcell_comment: {
        flexDirection: "row",
        marginTop: 5,
        width: Dimensions.get("window").width - 80,
        height: 38,
        marginBottom: 5
    },





});
