import React, { Component } from 'react';
//import Moment from "moment/min/moment-with-locales";
import {
    Dimensions,BackHandler,
    Animated, Easing, ImageBackground, Image, WebView,
    View, StyleSheet, TextInput, TouchableOpacity, Text, ScrollView
} from 'react-native';
import { Table, Row, Rows } from 'react-native-table-component';
import Timer_view from './user_timer';
//import {observer} from 'mobx-react';
import TaskData from '../../src/Store'

//@observer
export default class tasklisting extends Component {
    static navigationOptions = {
        title: null,
        header: null,

    };
    constructor(props) {
        super(props);

        const result = this.props.navigation.getParam('result');
        const task_data = this.props.navigation.getParam('task_data');

        this.state = {
            user_information: '',
            Username: '',
            password: '',
            tableHead: ['13/12/18', '1:20:00', '2'],
            result: result,
            task_data:task_data,
            countdown: 0,
            count_sec_Right:0,
            testFlag:false,
          
        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
     
        this._onpress_task_profile = this._onpress_task_profile.bind(this)
      
    }

    handleBackButtonClick() {
    this.props.navigation.goBack(null);
    let res = TaskData.todoList;
    this.setState({result:res,task_data:res.tasks ,testFlag:true})
    this.componentDidMount();
   //  alert(this.props.navigation.isFocused())
     return true;
 }
 componentWillMount() {
     BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
 }
 componentWillUnmount() {
   
     BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);  
 }
 componentDidMount() {
     let res = TaskData.todoList;
     TaskData.test22();
     this.setState({result:res,task_data:res.tasks })
  
   
 }
    _onpress_time_on(task_data) {
        //this.state.task_data=task_data
    }
    _onpress_dashboard() {
        this.props.navigation.navigate('User_dash')

    }
    _onpress_task_profile(result, item,state) {

       
        this.props.navigation.navigate('user_taskprofilepage', { result: result, item: item ,state:state})
       
    }
    _onpress_working_list() {
        
         this.props.navigation.navigate('user_Currently_list', { result:this.state.result,task_data:this.state.task_data })
   }



    render() {
        const result = this.props.navigation.getParam('result');
        const Task_list_data = result.tasks;




        return (

            <View style={styles.container}>
                <View style={styles.up_stair}>

                    <View style={styles.up_title}>
                        <View style={styles.upt_left}>
                            <Image source={require('../../assets/images/logo.jpg')} style={styles.welcomeImage} />
                        </View>
                        <View style={styles.upt_right}>
                            <Text style={styles.head_title}>TASK LISTING</Text>
                        </View>
                    </View>
                    <ScrollView>
                        {/*-----------------------------------------*/}

                        {Task_list_data.map((item, index) => (
                             <View>
                             <Timer_view
                                 result={result}
                                 item={item}
                                 name={item.name}
                                 end_date={item.end_date}
                                 spent_time={item.spent_time}
                                 users_number={item.users_number}
                                 status={item.status}
                                 ongoing_status={item.ongoing_status}
                                 start_time={item.start_time}
                                 priority={item.priority}
                                 username={result.data.name}
                                 password={result.data.password}
                                 task_id={item.id}
                                 _onpress_task_profile={this._onpress_task_profile}
                                 _ontimeer={this._onpress_time_on}
                                 testFlag = {this.state.testFlag}

                             />
                         </View>
                        ))
                        }
                        {/*-----------------------------------------*/}
                    </ScrollView>


                </View>
                <View style={styles.down_stair}>
                    <View style={styles.line} />
                    <View style={styles.button_container}>

                        <TouchableOpacity style={styles.btnContainer1}
                            onPress={() => this._onpress_working_list()}>

                            <Text style={styles.text_logout}>WORKING</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btnContainer2}
                            onPress={() => this._onpress_dashboard()}>

                            <Text style={styles.text_logout}>DASHBOARD</Text>
                        </TouchableOpacity>
                    </View>
                </View>






            </View>

        );
    }
}
const styles = StyleSheet.create({
    welcomeImage: {
        marginTop: 5,
        marginBottom: 5,

        width: (Dimensions.get("window").width - 90) / 2,
        height: (Dimensions.get("window").width - 90) / 2,
        borderRadius: 100,
    },
    user_image: {
        width: Dimensions.get("window").width - 50,
        justifyContent: 'center',
        alignItems: 'center',


    },
    head: {

        height: 43
    },
    up_title: {
        marginTop: 10,
        width: Dimensions.get("window").width - 50,
        marginBottom: 30,
        flexDirection: 'row',

    },
    upt_left: {
        flex: 1,
        height: 90,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(35,31,32,1)',
        marginRight: 10



    },
    upt_right: {
        flex: 3,
        height: 90,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(87,88,90,1)',



    },

    container: {
        flexDirection: 'column',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(209,210,212,1)',
        height: Dimensions.get("window").height,

    },
    up_stair: {
        flex: 8,
        marginBottom: 1,
        height: 100,
    },
    down_stair: {
        marginTop: 0,
        flex: 2

    },
    textcontainer: {
        marginTop: 3,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: Dimensions.get("window").width - 50,

    },
    button_container: {
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',

        width: Dimensions.get("window").width - 50,

    },
    title: {
        alignItems: "center",
        fontSize: 22,
    },

    container_upstair: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',



    },
    container_downstair: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'flex-start',

    },



    btnContainer1: {
        marginTop: 10,
        marginLeft: 4,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(13,147,68,1)',
        height: 39,
        borderRadius: 5,
        zIndex: 100,
        width: 110,
    },
    btnContainer2: {
        marginTop: 10,
        marginLeft: 4,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(87,88,90,1)',
        height: 39,
        borderRadius: 5,
        zIndex: 100,
        width: 110,
    },


    text: {
        marginLeft: 10,
        color: 'white',
        fontSize: 16,

    },
    lf_upcell: {
        marginLeft: 10,
        marginTop: 10,
        color: 'white',
        fontSize: 20,
        justifyContent: 'center',


    },
    table_text: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'black',
        fontSize: 12,

    },
    head_title: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'white',
        fontSize: 24,

    },
    text_logout: {
        marginLeft: 0,
        color: 'white',
        fontSize: 16,

    },

    line: {
        marginTop: 20,
        borderColor: 'black',
        borderWidth: 1,
        width: Dimensions.get("window").width - 50,
        height: 2,
        backgroundColor: 'rgba(0,0,0,1)',

    },
    //----------------------- //
    big_cell: {
        flexDirection: "row",
        width: Dimensions.get("window").width - 50,
        height: 100,
        marginBottom: 20

    },
    second_Lfcell: {
        flexDirection: "column",
        flex: 8,
        marginRight: 5,

    },
    second_Rtcell: {
        flex: 1,
        flexDirection: "column",
        marginLeft: 5,

    },
    third_Lf_downcell: {
        flex: 1,
        marginTop: 5,

        flexDirection: "row"
    },
    third_Lf_upcell: {
        flex: 1,
        marginBottom: 5,
        backgroundColor: 'rgba(128,130,133,1)',
    }



});
