import React, { Component } from 'react';

import {
    Dimensions, ScrollView,
    Animated, Easing, ImageBackground, Image, WebView,
    View, StyleSheet, TextInput, TouchableOpacity, Text
} from 'react-native';
import Timer_view from '../timer'
import * as constant from "../constant";
import {observer} from 'mobx-react';
import TaskData from '../../src/Store'
import { connect } from 'react-redux';

 class userdashboard extends Component {
    static navigationOptions = {
        title: null,
        header: null,
    };
    state = {
    };
    constructor(props) {
        super(props)
        const window = this.props.navigation.getParam('window_status')
        const result = this.props.navigation.getParam('result')
        this.state = {
            window_status: window,
            user_information: '',
            Username: '',
            password: '',
            result: result,
            task_data: result.tasks,
            start: '',
            ongoing_status: '',
            countdown: 0,
        }
        this._onpress_task_profile = this._onpress_task_profile.bind(this)
    }
    componentDidMount() {
        const { result } = this.props;
        const window = this.props.navigation.getParam('window_status')
        this.setState({ window_status: window,result:result,task_data:result.tasks })
    }
    _onpress_task_profile(result, item) {
        this.setState({ window_status: 0 })
        this.props.navigation.navigate('Task_profile', { result: result, item: item })
    }
    _onpress_user_profile() {
      
        this.props.navigation.navigate('User_userprofile', { user_item: this.state.result.users, result: this.state.result })
    }
    _onpress_time_on(task_data) {
        //this.state.task_data=task_data
    }
    _onpress_task_listing() {
        this._update_data(1);
       // this.setState({ window_status: 1 })
    }
    _onpress_log_listing() {
        this._update_data(3);
    }
    _onpress_completed_task(){
        this._update_data(5);
    }

    _onpress_working_list() {
        this._update_data(2);
    }
    _onpress_ongoing_task(){
        this._update_data(2);
    }
    _onpress_logout() {
        this.props.navigation.navigate('Login',{flag:true})

    }
    _update_data(number) {
            try {
            fetch(`${constant.domain}/data_update/`, {
                method: 'POST',
                header: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    username: this.state.result.data.name,
                    password: this.state.result.data.password,
                })
            })
                .then((response) => response.json())
                .then((res => {
                    this.setState({ task_data: res.tasks })
                    this.setState({ result: res })
                    switch (number) {
                         case 1:  this.props.navigation.navigate('User_task_listing', { result: res,task_data:res.tasks });break;
                         case 2: this.props.navigation.navigate('user_Currently_list', { result: res,task_data:res.tasks });break;
                         case 3:  this.props.navigation.navigate('user_log_listing', { result: res });break;
                         case 4: this.props.navigation.navigate('Logs_list', { result: res });break;
                         case 5: this.props.navigation.navigate('user_completed_listing', { result: res ,task_data:res.tasks });break;
                       }
                   })
                ).done();
        }
        catch (e) {
        }
    }
    select_header() {
        switch (this.state.window_status) {
            case 0:
                return <View style={styles.upt_right}>
                    <Text style={styles.head_title}>DASHBOARD</Text>
                </View>
            case 1:
                return <View style={styles.upt_right}>
                    <Text style={styles.head_title}>TASK LISTING</Text>
                </View>
            case 2:
                return <View style={styles.upt_right}>
                    <Text style={styles.head_title}>LOG LISTING</Text>
                </View>
            case 3:
                return <View style={styles.upt_right}>
                    <Text style={styles.head_title}>COMPLETED TASKS</Text>
                </View>
            case 4:
                return <View style={styles.upt_right}>
                    <Text style={styles.head_title}>CURRENTLY TASK</Text>
                </View>
        }
    }
    select_body() {
        let { result } = this.state;
        let { task_data } = this.state;
        switch (this.state.window_status) {
            case 0:
                return <View>
                     <View style={styles.user_image} >
                        <View style={styles.welcomeImage} >
                        <Text style={styles.text_surname}>{result.data.firstname.charAt(0) + result.data.lastname.charAt(0)}</Text>
                        </View>
                    </View>
                    <View style={styles.textcontainer}>
                        <Text style={styles.title_username}>{result.data.firstname + " " + result.data.lastname}</Text>
                    </View>
                    <View style={styles.textcontainer}>
                        <Text style={styles.title_deparment}>{result.data.department}</Text>
                    </View>

                    <TouchableOpacity borderWidth={2} style={styles.category_btnContainer1}

                        onPress={() => this._onpress_task_listing()}>

                        <Text style={styles.text}>Task listing</Text>
                    </TouchableOpacity>
                    <TouchableOpacity borderWidth={2} style={styles.category_btnContainer2}
                        onPress={() => this._onpress_log_listing()}>

                        <Text style={styles.text}>Logs listing</Text>
                    </TouchableOpacity>
                    <TouchableOpacity borderWidth={2} style={styles.category_btnContainer3}
                        onPress={() => this._onpress_user_profile()}>

                        <Text style={styles.text}>Profile</Text>
                    </TouchableOpacity>
                    <TouchableOpacity borderWidth={2} style={styles.category_btnContainer4}
                        onPress={() => this._onpress_completed_task()}>

                        <Text style={styles.text}>Completed Tasks</Text>
                    </TouchableOpacity>
                    <TouchableOpacity borderWidth={2} style={styles.category_btnContainer5}
                        onPress={() => this._onpress_ongoing_task()}>

                        <Text style={styles.text}>Ongoing Tasks</Text>
                    </TouchableOpacity>

                </View>

            case 1:
                return <ScrollView>
                    {/*-----------------------------------------*/}

                    {task_data.map((item, index) => (
                        <View>
                            <Timer_view
                                result={result}
                                item={item}
                                name={item.name}
                                end_date={item.end_date}
                                spent_time={item.spent_time}
                                users_number={item.users_number}
                                status={item.status}
                                ongoing_status={item.ongoing_status}
                                start_time={item.start_time}
                                priority={item.priority}
                                username={result.data.name}
                                password={result.data.password}
                                task_id={item.id}
                                _onpress_task_profile={this._onpress_task_profile}
                                _ontimeer={this._onpress_time_on}

                            />
                        </View>

                    ))
                    }
                    {/*-----------------------------------------*/}

                </ScrollView>
            case 3:
            return <ScrollView>
            {/*-----------------------------------------*/}

            {task_data.map((item, index) => {
                if(item.status=="F"){
                return<View>
                    <Timer_view
                        result={result}
                        item={item}
                        name={item.name}
                        end_date={item.end_date}
                        spent_time={item.spent_time}
                        users_number={item.users_number}
                        status={item.status}
                        ongoing_status={item.ongoing_status}
                        start_time={item.start_time}
                        priority={item.priority}
                        username={result.data.name}
                        password={result.data.password}
                        task_id={item.id}
                        _onpress_task_profile={this._onpress_task_profile}
                        _ontimeer={this._onpress_time_on}

                    />
                </View>
                }

            })
            }
            {/*-----------------------------------------*/}

        </ScrollView>
            case 2:
                return <ScrollView>
                    {/*-----------------------------------------*/}
                    {result.time_log.map((item, index) => {
                        if(this.state.result.data.id==item.user_id){
                        return<View style={styles.log_big_cell}>
                            <View style={styles.log_second_upcell}>
                                <Text style={styles.lf_upcell}>{"Job" + item.task_id}</Text>

                            </View>
                            <View style={styles.log_second_downcell} >
                                <View borderRadius={4} flex={8} borderWidth={2} marginRight={3} >
                                    <Text style={styles.content_text}>{item.date}</Text>
                                </View>
                                <View flex={6} borderRadius={4} marginRight={3} borderWidth={2}>
                                    <Text style={styles.content_text}>{item.time}</Text>
                                </View>
                                <View flex={4} borderRadius={4} marginRight={3} borderWidth={2}>
                                    <Text style={styles.content_text}>{"User" + item.user_id}</Text>
                                </View>
                                <View flex={3} borderRadius={4} justifyContent={'center'} alignItems={'center'} backgroundColor={item.task_status == "Q" ? 'rgba(247,147,29,1)' : 'rgba(0,165,79,1)'}>
                                    <Text style={styles.text_logout}>{item.task_status}</Text>
                                </View>
                            </View>
                        </View>
                        }
                    })}
                    {/*-----------------------------------------*/}
                </ScrollView>
            case 4:
                return <ScrollView>
                    {/*-----------------------------------------*/}
                    {task_data.map((item, index) => {
                        if (item.ongoing_status == "1") {
                            return <View>
                                <Timer_view
                                    result={result}
                                    item={item}
                                    name={item.name}
                                    end_date={item.end_date}
                                    spent_time={item.spent_time}
                                    users_number={item.users_number}
                                    status={item.status}
                                    ongoing_status={item.ongoing_status}
                                    start_time={item.start_time}
                                    priority={item.priority}
                                    username={result.data.name}
                                    password={result.data.password}
                                    task_id={item.id}
                                    _onpress_task_profile={this._onpress_task_profile}
                                    _ontimeer={this._onpress_time_on}
                                />
                            </View>
                        }
                    })
                    }
                    {/*-----------------------------------------*/}
                </ScrollView>
        }
    }
    select_tail() {
        if (this.state.window_status == 0) {
            return <View style={styles.button_container}>
                <TouchableOpacity style={styles.btnContainer}
                    onPress={() => this._onpress_logout()}>
                    <Text style={styles.text_logout}>Logout</Text>
                </TouchableOpacity>
            </View>
        }
        {
            return <View style={styles.button_container}>
                <TouchableOpacity style={styles.btnContainer1}
                    onPress={() => this._onpress_working_list()}>

                    <Text style={styles.text_logout}>WORKING</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btnContainer2}
                    onPress={() => this._onpress_dashboard()}>

                    <Text style={styles.text_logout}>DASHBOARD</Text>
                </TouchableOpacity>
            </View>
        }
    }
    _onpress_dashboard() {
        this.setState({ window_status: 0 })
    }
    render() {
        const result = this.props.navigation.getParam('result');
        return (
            <View style={styles.container}>
                <View style={styles.up_stair}>
                    <View style={styles.up_title}>
                        <View style={styles.up_title}>
                            <View style={styles.upt_left}>
                                <Image source={require('../../assets/images/logo.jpg')} style={{ resizeMode: 'stretch', flex: 1 }} />
                            </View>
                            {/*------------------------------------------------------ */}
                            {this.select_header()}
                            {/*-------------------------------------------------------- */}
                        </View>
                    </View>
                    {this.select_body()}
                </View>
                {/*-------------------------------------------------------------------*/}
                <View style={styles.down_stair}>
                    <View style={styles.line} />
                    {/*--------------------------------------------- */}
                    {this.select_tail()}
                    {/*--------------------------------------------- */}
                </View>
            </View>
        );    }
}
const styles = StyleSheet.create({
    welcomeImage: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 3,
        marginBottom: 3,
        backgroundColor: 'rgba(18,167,157,1)',
        width: (Dimensions.get("window").width - 120) / 2,
        height: (Dimensions.get("window").width - 120) / 2,
        borderRadius: 100,
        borderWidth:2,
        borderColor:'black',
    },
    user_image: {
        width: Dimensions.get("window").width - 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    up_title: {
        marginTop: 10,
        width: Dimensions.get("window").width - 50,
        marginBottom: 5,
        flexDirection: 'row',

    },
    upt_left: {
        flex: 1,
        height: 70,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(35,31,32,1)',
        marginRight: 10




    },
    upt_right: {
        flex: 3,
        height: 70,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(87,88,90,1)',



    },

    container: {
        flexDirection: 'column',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(209,210,212,1)',
        height: Dimensions.get("window").height,

    },
    up_stair: {
        flex: 10,
        marginBottom: 10,

    },
    down_stair: {
        marginTop: 10,
        flex: 2

    },
    textcontainer: {
        marginTop: 2,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: Dimensions.get("window").width - 50,

    },
    button_container: {
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',

        width: Dimensions.get("window").width - 50,

    },
    title: {
        alignItems: "center",
        fontSize: 22,
        color: 'black'
    },
    title_username: {
        alignItems: "center",
        fontSize: 22,
        //color:'black'
    },
    title_deparment: {
        alignItems: "center",
        fontSize: 24,
        color: 'black',
        fontWeight: 'bold',
    },
    head_title: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'white',
        fontSize: 24,

    },

    container_upstair: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    container_downstair: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    btnContainer: {
        marginTop: 5,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(255,2,2,0.8)',
        height: 39,
        borderRadius: 5,
        zIndex: 100,
        width: 90
    },
    category_btnContainer1: {
        marginTop: 10, alignItems: 'flex-start', justifyContent: 'center', width: Dimensions.get("window").width - 50,
        backgroundColor: 'rgba(16,116,188,1)',
        height: 39, borderRadius: 5, zIndex: 100, borderWidth: 1, borderColor: 'black'
    },
    category_btnContainer2: {
        marginTop: 10, alignItems: 'flex-start', justifyContent: 'center', width: Dimensions.get("window").width - 50,
        backgroundColor: 'rgba(18,167,157,1)',
        height: 39, borderRadius: 5, zIndex: 100, borderWidth: 1, borderColor: 'black'
    },
    category_btnContainer3: {
        marginTop: 10, alignItems: 'flex-start', justifyContent: 'center', width: Dimensions.get("window").width - 50,
        backgroundColor: 'rgba(138,93,59,1)',
        height: 39, borderRadius: 5, zIndex: 100, borderWidth: 1, borderColor: 'black'
    },
    category_btnContainer4: {
        marginTop: 10, alignItems: 'flex-start', justifyContent: 'center', width: Dimensions.get("window").width - 50,
        backgroundColor: 'rgba(238,29,35,1)',
        height: 39, borderRadius: 5, zIndex: 100, borderWidth: 1, borderColor: 'black'
    },
    category_btnContainer5: {
        marginTop: 10, alignItems: 'flex-start', justifyContent: 'center', width: Dimensions.get("window").width - 50,
        backgroundColor: 'rgba(13,147,68,1)',
        height: 39, borderRadius: 5, zIndex: 100, borderWidth: 1, borderColor: 'black'
    },
    text: {
        marginLeft: 10,
        color: 'white',
        fontSize: 16,
    },
    text_down: {
        marginTop: 10,
        color: 'black',
        fontSize: 20,
        marginBottom: 10,
    },
    line: {
        marginTop: 20,
        borderColor: 'black',
        borderWidth: 1,
        width: Dimensions.get("window").width - 50,
        height: 2,
        backgroundColor: 'rgba(0,0,0,1)',
    },
    //----------------------- //
    user_big_cell: {
        flexDirection: "row",
        width: Dimensions.get("window").width - 50,
        marginBottom: 3,
    },
    user_second_Lfcell: {
        flexDirection: "column",
        flex: 10,
        marginRight: 5,
        height: 46,
        justifyContent: 'center',
        borderRadius: 5,
    },
    user_second_Rtcell: {
        flex: 2,
        marginLeft: 5,
        height: 42,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
    },
    user_third_Lf_downcell: {
        flex: 1,
        marginTop: 5,

        flexDirection: "row"
    },
    user_third_Lf_upcell: {
        flex: 1,
        marginBottom: 5,
        backgroundColor: 'rgba(128,130,133,1)',
        borderRadius: 5,
    },
    text_users_Tasks: {
        marginLeft: 0,
        color: 'black',
        fontSize: 20,

    },
    number_Tasks: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'black',
        fontSize: 16,

    },
    lf_upcell: {
        marginLeft: 10,
        marginTop: 10,
        color: 'white',
        fontSize: 20,
        justifyContent: 'center',


    },
    btnContainer1: {
        marginTop: 10,
        marginLeft: 4,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(13,147,68,1)',
        height: 39,
        borderRadius: 5,
        zIndex: 100,
        width: 110,
    },
    btnContainer2: {
        marginTop: 10,
        marginLeft: 4,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(87,88,90,1)',
        height: 39,
        borderRadius: 5,
        zIndex: 100,
        width: 110,
    },
    text_logout: {
        marginLeft: 0,
        color: 'white',
        fontSize: 16,

    },
    ///------------------------------------------------------------
    log_big_cell: {
        flexDirection: "column",
        width: Dimensions.get("window").width - 50,
        //height: 50,
        marginBottom: 15,

    },
    time_log_text: {
        width: Dimensions.get("window").width - 50,
        marginBottom: 10,
        height: 20,

    },
    log_second_Lfcell: {
        flexDirection: "column",
        flex: 8,
        marginRight: 5,

    },
    log_second_upcell: {
        backgroundColor: 'rgba(128,130,133,1)',
        width: Dimensions.get("window").width - 50,
        height: 40,
        borderRadius: 5,


    },
    text_surname:{
        alignItems: "center",
        fontSize: 56,
        color:'black',
        fontWeight: 'bold',
    
    },
    
    log_second_downcell: {
        flexDirection: "row",
        marginTop: 5,
        width: Dimensions.get("window").width - 50,
        height: 40
    },
    content_text: {
        marginLeft: 10,
        marginTop: 10,
        color: 'black',
        fontSize: 20,
        justifyContent: 'center',
    },
});
const mapStateToProps = state => ({
    result: state.session.res
})

export default connect(mapStateToProps)(userdashboard);