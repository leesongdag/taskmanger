import React, { Component } from 'react';

import {
    Dimensions, ScrollView,
    Animated, Easing, ImageBackground, Image, WebView,
    View, StyleSheet, TextInput, TouchableOpacity, Text
} from 'react-native';
import Progress from 'react-native-progressbar';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import Timer_view from '../profile/profile_timer';
import Moment from 'moment';
import * as constant from "../constant";
import {observer} from 'mobx-react';
import TaskData from '../../src/Store'
import { Table, Row, Rows } from 'react-native-table-component';
export default class userlisting extends Component {
    static navigationOptions = {
        title: null,
        header: null,

    };
    constructor(props) {
        super(props);

        const result = this.props.navigation.getParam('result');
        const task_data = this.props.navigation.getParam('item');
        const state = this.props.navigation.getParam('state');

        // const task_data = this.props.navigation.getParam('item');

        this.state = {
            start: '',
            ongoing_status: '',
            working_status:state,
            spent_time: task_data.spent_time,
            add_comment_conent: '',
            select_user: '  Select user',
            select_user_id: '',
            user_information: '',
            Username: '',
            password: '',
            tableHead: ['13/12/18', '1:20:00', '2'],
            result: result,
            task_data: task_data,
            time_log: result.time_log,
            intervalReference: null,
            originalCountdown: 0,
            countdown: 0,
        };
        setInterval(() => {
            this.setState({ time: new Date().toLocaleTimeString() })
        }, 1000)
    }
    _menu = null;

    setMenuRef = ref => {
        this._menu = ref;
    };

    hideMenu(name, id) {
        this.setState({ select_user: name, select_user_id: id })

        this._menu.hide();
    };

    showMenu = () => {
        this._menu.show();
    };
    componentWillMount() {
         TaskData.test11();

        const task_data = this.props.navigation.getParam('item');
        //  alert(task_data.ongoing_status)
        this.setState({ ongoing_status: task_data.ongoing_status });
        if (task_data.ongoing_status == "1") {

            this.setState({ start: task_data.start_time })
            this.startCountDown();
        }
    }

    _onpress_dashboard() {
        if (this.state.result.priv == "admin")
            this.props.navigation.navigate('Dash', { window_status: 0 })
        else
            this.props.navigation.navigate('User_dash')

    }
    _onpress_working_list() {

        this.props.navigation.navigate('Dash', { result: this.state.result, window_status: 4 })
    }
    _onpress_ON() {

        let { result } = this.state;

        let { task_data } = this.state;

        if (this.state.ongoing_status == "0") {
            this.setState({ ongoing_status: 1 });
            const current_date = new Date();
            const time = current_date.toLocaleTimeString();


            console.log(time)
            this.setState({ start: time });
            this.refs.progress.show();

            try {
                fetch(`${constant.domain}/update_start_time/`, {
                    method: 'POST',
                    header: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        username: result.data.name,
                        password: result.data.password,
                        task_id: task_data.id,
                        task_start_time: time,
                        work_status: this.state.working_status,
                        date: current_date,
                    })
                })
                .then((response) => response.json())
                .then((res => {
                    TaskData.update(res);
                    TaskData.test1();
                    this.setState({ time_log: res.time_log });
                    this.render()
                    this.refs.progress.finish();


                })
                ).done();
            }
            catch (e) {
                this.refs.progress.finish();


            }


            this.startCountDown();
        }


    }
    pause() {
        clearInterval(this.state.intervalReference)
    }
    _onpress_off() {
        let { result } = this.state;

        let { task_data } = this.state;
        if (this.state.ongoing_status == "1") {
            this.pause();
            this.setState({ongoing_status:'0',spent_time:"00:00:00"})
            this.refs.progress.show();
            const current_date = new Date();
            //console.log(time)
            try {
                fetch(`${constant.domain}/add_time_log/`, {
                    method: 'POST',
                    header: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        username: result.data.name,
                        password: result.data.password,
                        task_id: task_data.id,
                        date: current_date,
                        work_status: this.state.working_status,
                        time: this.state.spent_time
                    })
                })
                .then((response) => response.json())
                .then((res => {
                  
                    TaskData.update(res);
                    TaskData.test1();
                    this.setState({ time_log: res.time_log });
                    //  alert(res.status_message)
                    //  this.setState({Task_list_data:res.tasks});

                    this.render()
                    this.refs.progress.finish();

                })
                ).done();
            }
            catch (e) {
                this.refs.progress.finish();


            }

        }

    }

   
  
    _onpress_add_comment() {
        if (this.state.add_comment_conent != "") {
            this.refs.progress.show();
            const current_date = new Date();
            try {
                fetch(`${constant.domain}/add_comment/`, {
                    method: 'POST',
                    header: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        username: this.state.result.data.name,
                        password: this.state.result.data.password,
                        task_id: this.state.task_data.id,
                        content: this.state.add_comment_conent,
                        date: current_date,
                    })
                })
                    .then((response) => response.json())
                    .then((res => {

                        this.setState({ task_data: res.task_profile_data, add_comment_conent: "" });

                        this.render()
                        this.refs.progress.finish();


                    })
                    ).done();
            }
            catch (e) {
                this.refs.progress.finish();

            }
        }
    }
    _onpress_del_comment(item) {
        this.refs.progress.show();
        try {
            fetch(`${constant.domain}/del_comment/`, {
                method: 'POST',
                header: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    username: this.state.result.data.name,
                    password: this.state.result.data.password,
                    task_id: this.state.task_data.id,
                    user_id: item.user_id,
                    date: item.date,
                    content: item.content,
                })
            })
                .then((response) => response.json())
                .then((res => {

                    this.setState({ task_data: res.task_profile_data });

                    this.render()
                    this.refs.progress.finish();

                })
                ).done();
        }
        catch (e) {
            this.refs.progress.finish();

        }
    }
  
   
    comment_btndel() {
        if (this.state.result.priv == "admin") {
            return <View flex={2} justifyContent={'center'} alignItems={'center'} backgroundColor='rgba(255,0,0,1)' borderRadius={4}>
                <TouchableOpacity
                    onPress={() => this._onpress_del_comment(item)}>

                    <Text style={styles.right_downcell}>DEL</Text>
                </TouchableOpacity>
            </View>
        }
    }
   
    _onpress_update_status(){
        let {status} = '';
        switch(this.state.working_status){
        case "Q" : status="W"; break;
        case "W" : status="C";break;
        case "C" :  status="Q" ;break;
        
        }
        this.refs.progress.show();
      
        try {
            fetch(`${constant.domain}/update_working_status/`, {
                method: 'POST',
                header: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    task_id: this.state.task_data.id,
                   task_status:status,
                })
            })
                .then((response) => response.json())
                .then((res => {
    
                    this.render()
                    this.refs.progress.finish();
                    this.setState({working_status:status})
    
                })
                ).done();
        }
        catch (e) {
            this.refs.progress.finish();
    
        }
    
    
    }
    

    render() {
        let { result } = this.state;

        let { task_data } = this.state;

        let { time_log } = this.state;



        return (

            <View style={styles.container}>
                <View style={styles.up_stair}>

                    <View style={styles.up_title}>
                        <View style={styles.upt_left}>
                            <Image source={require('../../assets/images/logo.jpg')} style={styles.welcomeImage} />
                        </View>
                        <View style={styles.upt_right}>
                            <Text style={styles.head_title}>TASK PROFILE</Text>
                        </View>
                    </View>

                    <ScrollView>


                        {/*----------------------------------------------------------------*/}
                        <View style={styles.big_cell} flexDirection={'row'}>
                            <View style={styles.second_Lfcell}>
                                <View style={styles.third_Lf_upcell} borderRadius={4}>
                                    <Text style={styles.lf_upcell}>{task_data.name}</Text>

                                </View>
                                <View style={styles.third_Lf_downcell}>
                                    <View flex={9} >
                                        <Table borderStyle={{ borderWidth: 2, borderColor: '#000' }}>
                                            <Row data={[task_data.end_date, this.state.spent_time, task_data.users_number]} textStyle={styles.table_text} />
                                        </Table>
                                    </View>
                                    <View   flex={2} marginLeft={3} fontSize={22} justifyContent={'center'} alignItems={'center'} backgroundColor={this.state.working_status == "Q" ? 'rgba(247,147,29,1)' : 'rgba(0,165,79,1)'}>
                                     
                                    <TouchableOpacity  flex={2} marginLeft={3} fontSize={22} justifyContent={'center'} alignItems={'center'} backgroundColor={this.state.working_status == "Q" ? 'rgba(247,147,29,1)' : 'rgba(0,165,79,1)'}
                                        onPress={() => this._onpress_update_status()}>

                                        <View   flex={2} marginLeft={3} fontSize={22} justifyContent={'center'} alignItems={'center'} backgroundColor={this.state.working_status == "Q" ? 'rgba(247,147,29,1)' : 'rgba(0,165,79,1)'}>
                                            <Text style={styles.text_status_priority}>{this.state.working_status}</Text>
                                        </View>
                                    </TouchableOpacity>
                                    </View>
                                    <View flex={2} marginLeft={3} fontSize={22} justifyContent={'center'} alignItems={'center'} backgroundColor='rgba(238,29,35,1)'>
                                        <Text style={styles.text_status_priority}>{task_data.priority}</Text>
                                    </View>

                                </View>

                            </View>

                            {/*-----------------------------------------*/}

                            <View style={styles.second_Rtcell}>
                                <View flex={1} backgroundColor={this.state.ongoing_status == "0" ? 'rgba(128,130,133,1)' : 'rgba(13,147,68,1)'} fontSize={22} justifyContent={'center'} alignItems={'center'}>
                                    <TouchableOpacity
                                        onPress={() => this._onpress_ON()}>

                                        <Text style={styles.text_status_priority}>ON</Text>
                                    </TouchableOpacity>

                                </View>
                                <View flex={1} backgroundColor={this.state.ongoing_status == "0" ? 'rgba(238,29,35,1)' : 'rgba(128,130,133,1)'} fontSize={22} justifyContent={'center'} alignItems={'center'}>
                                    <TouchableOpacity
                                        onPress={() => this._onpress_off()}>

                                        <Text style={styles.text_status_priority}>OFF</Text>
                                    </TouchableOpacity>

                                </View>

                            </View>


                        </View>




                     
                        {/*---------------------------------------- */}
                        <View style={styles.big_cell}>
                            <Text style={styles.text_users_Tasks}>Task title</Text>
                        </View>
                        <View style={styles.second_upcell}>
                            <Text style={styles.lf_upcell}>{task_data.name}</Text>

                        </View>

                        {/*-----------------------------------------*/}
                        {/*---------------------------------------- */}
                        <View style={styles.big_cell}>
                            <Text style={styles.text_users_Tasks}>Date</Text>
                        </View>
                        <View style={styles.second_upcell}>
                            <Text style={styles.lf_upcell}>{task_data.end_date}</Text>

                        </View>

                        {/*-----------------------------------------*/}
                        {/*---------------------------------------- */}
                        <View style={styles.big_cell}>
                            <Text style={styles.text_users_Tasks}>Description</Text>
                        </View>
                        <View style={styles.second_upcell}>
                            <Text style={styles.lf_upcell}>{task_data.task_des}</Text>

                        </View>

                        {/*-----------------------------------------*/}
                        {/*---------------------------------------- */}
                        <View style={styles.big_cell}>
                            <Text style={styles.text_users_Tasks}>Group</Text>
                        </View>
                        <View style={styles.second_upcell}>
                            <Text style={styles.lf_upcell}>{task_data.group}</Text>

                        </View>

                        {/*-----------------------------------------*/}
                        {/*---------------------------------------- */}
                        <View style={styles.big_cell}>
                            <Text style={styles.text_users_Tasks}>Users</Text>
                        </View>
                        {/*-----------------------------------------*/}
                     

                        {/*-----------------------------------------*/}
                        {/*-----------------------------------------*/}

                        {task_data.task_users_data.map((item, index) => (
                            <View style={styles.second_downcell} >
                                <View flex={3} borderWidth={1} marginRight={3} borderRadius={4}>
                                    <Text style={styles.text_users}>{item.user_name}</Text>

                                </View>
                               
                            </View>

                        ))}
                        {/*-----------------------------------------*/}

                        {/*---------------------------------------- */}
                        <View style={styles.big_cell}>
                            <Text style={styles.text_users_Tasks}>Comments</Text>
                        </View>

                        <TextInput style={styles.inContainer_username}
                            underlineColorAndroid="transparent"
                            placeholder='Comment content'
                            multiline={true}
                            onChangeText={(add_comment_conent) => this.setState({ add_comment_conent })}
                            value={this.state.add_comment_conent}
                        />

                        <TouchableOpacity
                            onPress={() => this._onpress_add_comment()}>

                            <View style={styles.comment_btncontainer}>

                                <Text style={styles.right_downcell}>ADD CMMENT</Text>

                            </View>
                        </TouchableOpacity>

                        {/*-----------------------------------------*/}
                        {task_data.task_comments.map((item, index) => (
                            <View style={styles.big_cell_commment}>

                                <Text style={styles.text_users_Tasks}>{item.content}</Text>
                                <View style={styles.line} />
                                <View style={styles.second_downcell_comment} >
                                    <View justifyContent={'center'} alignItems={'center'} flex={3} marginBottom={5} marginRight={3} >
                                        <Text style={styles.text_users_Tasks}>{item.date}</Text>

                                    </View>
                                    <View justifyContent={'center'} alignItems={'center'} flex={2} marginBottom={5} marginRight={3}>
                                        <Text style={styles.text_users_Tasks}>{item.user_id} </Text>

                                    </View>
                                    <TouchableOpacity
                                        onPress={() => this._onpress_del_comment(item)}>

                                        <View flex={1} justifyContent={'center'} alignItems={'center'} borderRadius={4} backgroundColor='rgba(255,0,0,1)'>

                                            <Text style={styles.right_downcell}>DEL</Text>
                                        </View>
                                    </TouchableOpacity>



                                </View>




                            </View>

                        ))}
                        <Progress ref={'progress'} title={'Connecting...'} />

                        {/*-----------------------------------------*/}
                        <View style={styles.big_cell}>
                            <Text style={styles.text_users_Tasks}>Time Logs</Text>
                        </View>
                        {time_log.map((item, index) => {
                            if (item.task_id == task_data.id) {
                                return <View style={styles.second_downcell} >
                                    <View flex={2} justifyContent={'center'} alignItems={'center'} borderWidth={2} marginRight={2} borderRadius={4} >
                                        <Text style={styles.text_users_Tasks}>{item.date}</Text>

                                    </View>
                                    <View flex={2} justifyContent={'center'} alignItems={'center'} borderWidth={2} marginRight={2} borderRadius={4} >
                                        <Text style={styles.text_users_Tasks}>{item.time}</Text>

                                    </View>
                                    <View flex={1} justifyContent={'center'} alignItems={'center'} borderWidth={2} marginRight={1} borderRadius={4}  >
                                        <Text style={styles.text_users_Tasks}>{item.user_name}</Text>

                                    </View>


                                </View>
                            }
                        })}





                        {/*-----------------------------------------*/}

                    </ScrollView>
                </View>
                <View style={styles.down_stair}>
                    <View style={styles.line} />
                    <View style={styles.button_container}>


                        <TouchableOpacity style={styles.btnContainer2}
                            onPress={() => this._onpress_dashboard()}>

                            <Text style={styles.text_logout}>DASHBOARD</Text>
                        </TouchableOpacity>
                    </View>
                </View>






            </View>

        );
    }
    async startCountDown(countdownTime = this.state.countdown) {
        clearInterval(this.state.intervalReference)
        this.setState({ countdown: countdownTime, originalCountdown: countdownTime }, () => {
            let intervalReference = setInterval(() => {
                if (this.state.count_sec_Right < 10)
                    this.setState({ countdown: ++this.state.count_sec_Right })
                const now = new Date().toLocaleTimeString();
                const a = Moment(now, ("HH:mm:ss"));//;
                const b = Moment(this.state.start, ("HH:mm:ss"))
                const mins = Moment.utc(Moment(now, "HH:mm:ss").diff(Moment(this.state.start, "HH:mm:ss"))).format("HH:mm:ss")
                //const  second =Moment.utc(Moment(now,"HH:mm:ss").diff(Moment(this.state.start,"HH:mm:ss"))).format("ss")

                //  const hours=Moment.utc(b.diff(a,'hours'))

                this.setState({ spent_time: mins });


            }, 1000)

            this.setState({ intervalReference: intervalReference })
        })
    }
}
const styles = StyleSheet.create({
    welcomeImage: {
        marginTop: 5,
        marginBottom: 5,

        width: (Dimensions.get("window").width - 90) / 2,
        height: (Dimensions.get("window").width - 90) / 2,
        borderRadius: 100,
    },
    user_image: {
        width: Dimensions.get("window").width - 50,
        justifyContent: 'center',
        alignItems: 'center',


    },
    head: {

        height: 43
    },
    up_title: {
        marginTop: 10,
        width: Dimensions.get("window").width - 50,
        marginBottom: 10,
        flexDirection: 'row',

    },
    upt_left: {
        flex: 1,
        height: 90,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(35,31,32,1)',
        marginRight: 10



    },
    upt_right: {
        flex: 3,
        height: 90,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(87,88,90,1)',



    },

    container: {
        flexDirection: 'column',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(209,210,212,1)',
        height: Dimensions.get("window").height,

    },
    up_stair: {
        flex: 8,
        marginBottom: 10,
        height: 100,
    },
    down_stair: {
        marginTop: 0,
        flex: 2

    },
    textcontainer: {
        marginTop: 3,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: Dimensions.get("window").width - 50,

    },
    button_container: {
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',

        width: Dimensions.get("window").width - 50,

    },
    title: {
        alignItems: "center",
        fontSize: 22,
    },

    container_upstair: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',



    },
    container_downstair: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'flex-start',

    },



    btnContainer1: {
        marginTop: 10,
        marginLeft: 4,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(13,147,68,1)',
        height: 39,
        borderRadius: 5,
        zIndex: 100,
        width: 110,
    },
    btnContainer2: {
        marginTop: 10,
        marginLeft: 4,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(87,88,90,1)',
        height: 39,
        borderRadius: 5,
        zIndex: 100,
        width: 110,
    },


    text: {
        marginLeft: 10,
        color: 'white',
        fontSize: 16,

    },
    right_downcell: {
        alignItems: 'center',
        color: 'white',
        fontSize: 20,
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5


    },
    lf_upcell: {
        marginLeft: 10,
        marginTop: 10,
        color: 'white',
        fontSize: 20,
        justifyContent: 'center',


    },
    spent_time_text: {
        alignItems: 'center',
        justifyContent: 'center',
        color: 'black',
        fontSize: 20,


    },
    priority_text: {
        alignItems: 'center',
        justifyContent: 'center',
        color: 'white',
        fontSize: 20,


    },
    table_text: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'black',
        fontSize: 12,

    },
    number_Tasks: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'black',
        fontSize: 16,

    },
    head_title: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'white',
        fontSize: 24,

    },
    text_logout: {
        marginLeft: 0,
        color: 'white',
        fontSize: 16,

    },
    text_users_Tasks: {
        marginLeft: 0,
        color: 'black',
        fontSize: 20,
        // marginLeft: 10,

    },
    text_users: {
        marginLeft: 0,
        color: 'black',
        fontSize: 20,
        marginLeft: 10,

    },

    line: {
        marginTop: 20,
        borderColor: 'black',
        borderWidth: 1,
        width: Dimensions.get("window").width - 50,
        height: 2,
        backgroundColor: 'rgba(0,0,0,1)',

    },
    //------------------------//
    second_upcell: {
        backgroundColor: 'rgba(128,130,133,1)',
        width: Dimensions.get("window").width - 50,
        height: 40,
        borderRadius: 5,
        marginBottom: 5,


    },
    comment_btncontainer: {
        backgroundColor: 'rgba(128,130,133,1)',
        width: Dimensions.get("window").width - 50,
        height: 40,
        borderRadius: 5,
        marginBottom: 5,
        alignItems: 'center',
        justifyContent: 'center',


    },
    second_downcell: {
        flexDirection: "row",
        marginTop: 5,
        width: Dimensions.get("window").width - 50,
        height: 40,
        marginBottom: 5
    },
    //----------------------- //
    big_cell: {
        width: Dimensions.get("window").width - 50,
        marginBottom: 2,
        marginTop: 10,
    },
    big_row_cell: {
        flexDirection: "row",
        width: Dimensions.get("window").width - 50,
        height: 50,

    },
    second_Lfcell: {
        flexDirection: "column",
        flex: 6,
        marginRight: 5,

    },
    //---------comment-------------- //
    big_cell_commment: {
        flexDirection: "column",
        width: Dimensions.get("window").width - 50,
        height: 120,
        borderWidth: 2,
        borderRadius: 10,
        padding: 5,
        marginBottom: 10,
    },
    second_downcell_comment: {
        flexDirection: "row",
        marginTop: 5,
        width: Dimensions.get("window").width - 80,
        height: 38,
        marginBottom: 5
    },
    inContainer_username: {
        // alignItems: 'center',
        height: 80, width: Dimensions.get("window").width - 50, borderColor: 'rgba(0,0,0,1)', borderWidth: 1,
        marginTop: 5,
        fontSize: 20,
        backgroundColor: 'rgba(255,255,255,1)',
        borderRadius: 5,
        textAlign: 'left',
        //fontFamily: 'myanmar',
        color: 'black',
    },
    third_Lf_downcell: {
        flex: 1,
        marginTop: 5,
        height: 40,

        flexDirection: "row"
    },
    second_Rtcell: {
        flex: 1,
        flexDirection: "column",
        marginLeft: 5,

    },
    third_Lf_upcell: {
        flex: 1,
        marginBottom: 5,
        backgroundColor: 'rgba(128,130,133,1)',
    },
    text_status_priority: {
        marginLeft: 3,
        color: 'white',
        fontSize: 20,
        marginRight:3,

    },
    table_text: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'black',
        fontSize: 14,
        marginTop: 10,
        marginBottom: 10,

    },





});
