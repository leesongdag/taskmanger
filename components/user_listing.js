import React, { Component } from 'react';

import {
    Dimensions,
    Animated, Easing, ImageBackground, Image, WebView,
    View, StyleSheet, TextInput, TouchableOpacity, Text
} from 'react-native';
import { Table, Row, Rows } from 'react-native-table-component';
import * as constant from "./constant";
export default class userlisting extends Component {
    static navigationOptions = {
        title: null,
        header: null,

    };
    constructor(props) {
        super(props);

        const result = this.props.navigation.getParam('result');
        const task_data = this.props.navigation.getParam('task_data');

        this.state = {
            user_information: '',
            Username: '',
            password: '',
            tableHead: ['13/12/18', '1:20:00', '2'],
            result: result,
            task_data:task_data,
          
        };
     
      
    }
  
   
    componentWillMount() {

    }
    _onpress_dashboard() {
        this.props.navigation.navigate('Dash')

    }
    _onpress_user_profile(item) {
        const result = this.props.navigation.getParam('result');

        this.props.navigation.navigate('User_profile', { user_item: item, result: result })
    }
    _onpress_working_list() {
        const result = this.props.navigation.getParam('result');

        this.props.navigation.navigate('Currently_list',{result:result,task_data:result.tasks})
    }



    render() {

        const result = this.props.navigation.getParam('result');
     



        return (
            <View style={styles.container}>

                <View style={styles.up_stair}>
                    <View style={styles.up_title}>
                        <View style={styles.up_title}>
                            <View style={styles.upt_left}>
                                <Image source={require('../assets/images/logo.jpg')} style={{ resizeMode: 'stretch', flex: 1 }} />
                            </View>
                            {/*-----------------  {this.select_header()}------------------------------------- */}
                            <View style={styles.upt_right}>
                                <Text style={styles.head_title}>USER LISTING</Text>
                            </View>
                            {/*-------------------------------------------------------- */}

                        </View>

                    </View>
                    {/*----------------------  {this.select_body()}---------------------------------- */}

                    <View>
                        <View style={styles.user_big_cell}>
                            <View style={styles.user_second_Lfcell}>
                                <Text style={styles.text_users_Tasks}>Users</Text>

                            </View>


                            <View style={styles.user_second_Rtcell}>
                                <Text style={styles.text_users_Tasks}>Tasks</Text>

                            </View>



                        </View>
                        {/*-----------------------------------------*/}

                        {result.users.map((item, index) => {

                            if (item.priv != "adm") {
                                return <View style={styles.user_big_cell}>
                                    <View style={styles.user_second_Lfcell}>
                                        <View style={styles.user_third_Lf_upcell}>
                                            <TouchableOpacity
                                                onPress={() => this._onpress_user_profile(item)}>

                                                <Text style={styles.lf_upcell}>{item.username}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <View style={styles.user_second_Rtcell} borderWidth={2}>
                                        <Text style={styles.number_Tasks}  >{item.tasks_number}</Text>

                                    </View>



                                </View>
                            }
                        })
                        }
                        {/*-----------------------------------------*/}
                    </View>
                </View>

                {/*-------------------------------------------------------------------*/}
                <View style={styles.down_stair}>
                    <View style={styles.line} />
                    {/*-------------  {this.select_tail()}-------------------------------- */}
                    <View style={styles.button_container}>

                        <TouchableOpacity style={styles.btnContainer1}
                            onPress={() => this._onpress_working_list()}>

                            <Text style={styles.text_logout}>WORKING</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btnContainer2}
                            onPress={() => this._onpress_dashboard()}>

                            <Text style={styles.text_logout}>DASHBOARD</Text>
                        </TouchableOpacity>
                    </View>

                    {/*--------------------------------------------- */}

                </View>





            </View>

        );
    }
}
const styles = StyleSheet.create({
    welcomeImage: {
        marginTop: 3,
        marginBottom: 3,

        width: (Dimensions.get("window").width - 120) / 2,
        height: (Dimensions.get("window").width - 120) / 2,
        borderRadius: 100,
    },
    user_image: {
        width: Dimensions.get("window").width - 50,
        justifyContent: 'center',
        alignItems: 'center',


    },
    up_title: {
        marginTop: 10,
        width: Dimensions.get("window").width - 50,
        marginBottom: 5,
        flexDirection: 'row',

    },
    upt_left: {
        flex: 1,
        height: 70,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(35,31,32,1)',
        marginRight: 10




    },
    upt_right: {
        flex: 3,
        height: 70,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(87,88,90,1)',



    },

    container: {
        flexDirection: 'column',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(209,210,212,1)',
        height: Dimensions.get("window").height,

    },
    up_stair: {
        flex: 10,
        marginBottom: 10,

    },
    down_stair: {
        marginTop: 10,
        flex: 2

    },
    textcontainer: {
        marginTop: 2,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: Dimensions.get("window").width - 50,

    },
    button_container: {
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',

        width: Dimensions.get("window").width - 50,

    },
    title: {
        alignItems: "center",
        fontSize: 22,
        color: 'black'
    },
    title_username: {
        alignItems: "center",
        fontSize: 22,
        color:'black'
    },
    title_deparment: {
        alignItems: "center",
        fontSize: 24,
        color: 'black',
        fontWeight: 'bold',
    },
    head_title: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'white',
        fontSize: 24,

    },

    container_upstair: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',



    },
    container_downstair: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'flex-start',




    },



    btnContainer: {
        marginTop: 5,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(255,2,2,0.8)',
        height: 39,
        borderRadius: 5,
        zIndex: 100,
        width: 90
    },


    text: {
        marginLeft: 10,
        color: 'white',
        fontSize: 20,

    },
    text_down: {
        marginTop: 10,
        color: 'black',
        fontSize: 20,
        marginBottom: 10,

    },
    line: {
        marginTop: 20,
        borderColor: 'black',
        borderWidth: 1,
        width: Dimensions.get("window").width - 50,
        height: 2,
        backgroundColor: 'rgba(0,0,0,1)',

    },
    //----------------------- //
    user_big_cell: {
        flexDirection: "row",
        width: Dimensions.get("window").width - 50,
        marginBottom: 3,

    },
    user_second_Lfcell: {
        flexDirection: "column",
        flex: 10,
        marginRight: 5,
        height: 46,
        justifyContent: 'center',
        borderRadius: 5,


    },
    user_second_Rtcell: {
        flex: 2,
        marginLeft: 5,
        height: 42,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,

    },
    user_third_Lf_downcell: {
        flex: 1,
        marginTop: 5,

        flexDirection: "row"
    },
    user_third_Lf_upcell: {
        flex: 1,
        marginBottom: 5,
        backgroundColor: 'rgba(128,130,133,1)',
        borderRadius: 5,
    },
    text_users_Tasks: {
        marginLeft: 0,
        color: 'black',
        fontSize: 20,

    },
    number_Tasks: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'black',
        fontSize: 16,

    },
    lf_upcell: {
        marginLeft: 10,
        marginTop: 10,
        color: 'white',
        fontSize: 20,
        justifyContent: 'center',


    },
    btnContainer1: {
        marginTop: 10,
        marginLeft: 4,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(13,147,68,1)',
        height: 39,
        borderRadius: 5,
        zIndex: 100,
        width: 110,
    },
    btnContainer2: {
        marginTop: 10,
        marginLeft: 4,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(87,88,90,1)',
        height: 39,
        borderRadius: 5,
        zIndex: 100,
        width: 110,
    },
    text_logout: {
        marginLeft: 0,
        color: 'white',
        fontSize: 16,

    },
    ///------------------------------------------------------------
    log_big_cell: {
        flexDirection: "column",
        width: Dimensions.get("window").width - 50,
        //height: 50,
        marginBottom: 15,

    },
    time_log_text: {
        width: Dimensions.get("window").width - 50,
        marginBottom: 10,
        height: 20,

    },
    log_second_Lfcell: {
        flexDirection: "column",
        flex: 8,
        marginRight: 5,

    },
    log_second_upcell: {
        backgroundColor: 'rgba(128,130,133,1)',
        width: Dimensions.get("window").width - 50,
        height: 40,
        borderRadius: 5,


    },
    log_second_downcell: {
        flexDirection: "row",
        marginTop: 5,
        width: Dimensions.get("window").width - 50,
        height: 40
    },
    content_text: {
       // marginLeft: 10,
        marginTop: 10,
        color: 'black',
        fontSize: 20,
        justifyContent: 'center',
           alignItems: 'center',
     


    },


});
