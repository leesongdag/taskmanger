import React, { Component } from 'react';
import {
    Dimensions,
    Animated, Easing, ImageBackground, Image, WebView,
    View, StyleSheet, TextInput, TouchableOpacity, Text
} from 'react-native';
import { Table, Row, Rows } from 'react-native-table-component';
import * as constant from "./constant";
export default class workinglisting extends Component {
    static navigationOptions = {
        title: null,
        header: null,

    };
    state = {
        user_information: '',
        Username: '',
        password: '',
        tableHead: ['13/12/18', '1:20:00', '2'],
    };

    componentWillMount() {

        // this.setState({user_information});
        //  this._detect_login(user_information);
    }
    _onpress_dashboard() {
        this.props.navigation.navigate('Dash')

    }
    _onpress_task_profile() {
        this.props.navigation.navigate('Task_profile')
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.up_stair}>
                    <View style={styles.up_title}>
                        <View style={styles.upt_left}>
                            <Text style={styles.text_logout}>LOGO PNG</Text>
                        </View>
                        <View style={styles.upt_right}>
                            <Text style={styles.head_title}>Working LISTING</Text>
                        </View>
                    </View>
                    {/*-----------------------------------------*/}
                    <View style={styles.big_cell}>
                        <View style={styles.second_Lfcell}>
                            <View style={styles.third_Lf_upcell}>
                                <TouchableOpacity
                                    onPress={() => this._onpress_task_profile()}>
                                    <Text style={styles.lf_upcell}>JOB 4</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.third_Lf_downcell}>
                                <View flex={3}>
                                    <Table borderStyle={{ borderWidth: 2, borderColor: '#000' }}>
                                        <Row data={this.state.tableHead} style={styles.head} textStyle={styles.table_text} />
                                    </Table>
                                </View>
                                <View flex={1} marginLeft={3} fontSize={22} justifyContent={'center'} alignItems={'center'} backgroundColor='rgba(247,147,29,1)'>
                                    <Text style={styles.text_logout}>Q</Text>
                                </View>
                                <View flex={1} marginLeft={3} fontSize={22} justifyContent={'center'} alignItems={'center'} backgroundColor='rgba(238,29,35,1)'>
                                    <Text style={styles.text_logout}>H</Text>
                                </View>
                            </View>
                        </View>
                        {/*-----------------------------------------*/}
                        <View style={styles.second_Rtcell}>
                            <View flex={1} backgroundColor='rgba(128,130,133,1)' fontSize={22} justifyContent={'center'} alignItems={'center'}>
                                <Text style={styles.text_logout}>ON</Text>
                            </View>
                            <View flex={1} backgroundColor='rgba(238,29,35,1)' fontSize={22} justifyContent={'center'} alignItems={'center'}>
                                <Text style={styles.text_logout}>OFF</Text>
                            </View>
                        </View>
                    </View>
                    {/*-----------------------------------------*/}
                </View>
                <View style={styles.down_stair}>
                    <View style={styles.line} />
                    <View style={styles.button_container}>

                        <TouchableOpacity style={styles.btnContainer1}
                            onPress={() => this._onpress_login()}>
                            <Text style={styles.text_logout}>WORKING</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btnContainer2}
                            onPress={() => this._onpress_dashboard()}>
                            <Text style={styles.text_logout}>DASHBOARD</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );    
    }
}
const styles = StyleSheet.create({
    welcomeImage: {
        marginTop: 5,
        marginBottom: 5,
        width: (Dimensions.get("window").width - 90) / 2,
        height: (Dimensions.get("window").width - 90) / 2,
        borderRadius: 100,
    },
    user_image: {
        width: Dimensions.get("window").width - 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    head: {
        height: 43
    },
    up_title: {
        marginTop: 10,
        width: Dimensions.get("window").width - 50,
        marginBottom: 30,
        flexDirection: 'row',
    },
    upt_left: {
        flex: 1,
        height: 90,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(35,31,32,1)',
        marginRight: 10
    },
    upt_right: {
        flex: 3,
        height: 90,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(87,88,90,1)',



    },

    container: {
        flexDirection: 'column',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(209,210,212,1)',
        height: Dimensions.get("window").height,

    },
    up_stair: {
        flex: 8,
        marginBottom: 30,
        height: 100,
    },
    down_stair: {
        marginTop: 10,
        flex: 2

    },
    textcontainer: {
        marginTop: 3,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: Dimensions.get("window").width - 50,

    },
    button_container: {
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',

        width: Dimensions.get("window").width - 50,

    },
    title: {
        alignItems: "center",
        fontSize: 22,
    },

    container_upstair: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',



    },
    container_downstair: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'flex-start',

    },



    btnContainer1: {
        marginTop: 10,
        marginLeft: 4,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(13,147,68,1)',
        height: 39,
        borderRadius: 5,
        zIndex: 100,
        width: 110,
    },
    btnContainer2: {
        marginTop: 10,
        marginLeft: 4,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(87,88,90,1)',
        height: 39,
        borderRadius: 5,
        zIndex: 100,
        width: 110,
    },


    text: {
        marginLeft: 10,
        color: 'white',
        fontSize: 16,

    },
    lf_upcell: {
        marginLeft: 10,
        marginTop: 10,
        color: 'white',
        fontSize: 20,
        justifyContent: 'center',


    },
    table_text: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'black',
        fontSize: 12,

    },
    head_title: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'white',
        fontSize: 24,

    },
    text_logout: {
        marginLeft: 0,
        color: 'white',
        fontSize: 16,

    },

    line: {
        marginTop: 20,
        borderColor: 'black',
        borderWidth: 1,
        width: Dimensions.get("window").width - 50,
        height: 2,
        backgroundColor: 'rgba(0,0,0,1)',

    },
    //----------------------- //
    big_cell: {
        flexDirection: "row",
        width: Dimensions.get("window").width - 50,
        height: 100,

    },
    second_Lfcell: {
        flexDirection: "column",
        flex: 6,
        marginRight: 5,

    },
    second_Rtcell: {
        flex: 1,
        flexDirection: "column",
        marginLeft: 5,

    },
    third_Lf_downcell: {
        flex: 1,
        marginTop: 5,

        flexDirection: "row"
    },
    third_Lf_upcell: {
        flex: 1,
        marginBottom: 5,
        backgroundColor: 'rgba(128,130,133,1)',
    }



});
